package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.sign_in;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.BaseFragment;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.databinding.FragmentSignInBinding;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;

/**
 * Fragment class managing the sign in screen.
 */
public class SignInFragment extends BaseFragment {

    // === ATTRIBUTES

    private FragmentSignInBinding binding;


    // === FRAGMENT OVERRIDES

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_in, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbarVisibility = View.GONE;

        SignInViewModel viewModel = new ViewModelProvider(this).get(SignInViewModel.class);
        NavController navController = NavHostFragment.findNavController(this);

        binding.buttonCreateAccount.setOnClickListener(v -> navController.navigate(R.id.action_destination_sign_in_to_destination_sign_up));

        binding.buttonSignIn.setOnClickListener(v -> {
            String email = binding.editSignInEmail.getText().toString();
            String password = binding.editSignInPassword.getText().toString();
            boolean remember = binding.checkRememberMe.isChecked();
            viewModel.signIn(email, password, remember);
        });

        viewModel.getResult().observe(getViewLifecycleOwner(), firebaseResult -> {
            if (firebaseResult == null) return;

            if (firebaseResult == FirebaseResultState.SUCCESS) {
                navController.navigate(R.id.action_destination_sign_in_to_destination_home);
            } else if (firebaseResult == FirebaseResultState.FAILURE) {
                Toast.makeText(getContext(), getString(R.string.toast_error_sign_in), Toast.LENGTH_SHORT).show();
            }
        });

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
    }
}