package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums;

/**
 * Enumeration for the state of a game
 */
public enum GameState {
    WAITING,
    PLAYING,
    WON,
    LOST
}
