package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.game_maps;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.Marker;

import java.util.List;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.GameTimer;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Game;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Round;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Step;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.GameRepository;

import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.STEP_DURATION;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.TIMER_UPDATES;

/**
 * ViewModel class linked to the game screen including the Google Maps
 */
public class GameMapsViewModel extends AndroidViewModel implements GameTimer.OnGameTimerEvent {

    // === ATTRIBUTES

    private final Application application;
    private final GameRepository gameRepository;

    private final MutableLiveData<Boolean> replay = new MutableLiveData<>();
    private final MutableLiveData<Integer> countdown = new MutableLiveData<>();
    private final MutableLiveData<String> cityName = new MutableLiveData<>();
    private final MutableLiveData<String> countryName = new MutableLiveData<>();

    private final Game currentGame;
    private final Round currentRound;
    private final GameTimer gameTimer;

    private final List<Step> steps;
    private int currentStep = 0;
    private Marker currentMarker;


    // === CONSTRUCTOR

    public GameMapsViewModel(@NonNull Application application, boolean replay) {
        super(application);
        this.application = application;
        this.replay.setValue(replay);
        gameRepository = GameRepository.getInstance();

        gameTimer = GameTimer.getInstance(this);
        gameRepository.getCurrentRound().getValue().getSteps();

        currentGame = gameRepository.getCurrentGame().getValue();
        currentRound = gameRepository.getCurrentRound().getValue();
        steps = currentRound.getSteps();
    }


    // === GETTERS AND SETTERS

    public LiveData<Boolean> isReplay() {
        return replay;
    }

    public void setCurrentMarker(Marker currentMarker) {
        this.currentMarker = currentMarker;
    }

    public Marker getCurrentMarker() {
        return currentMarker;
    }

    public LiveData<String> getCityName() {
        return cityName;
    }

    public LiveData<Integer> getCountdown() {
        return countdown;
    }

    public LiveData<String> getCountryName() {
        return countryName;
    }

    public Round getCurrentRound() {
        return currentRound;
    }

    public Game getCurrentGame() {
        return currentGame;
    }


    // === PUBLIC METHODS

    public void validateStep() {
        gameTimer.stopTimer();
        double latitude = currentMarker.getPosition().latitude;
        double longitude = currentMarker.getPosition().longitude;
        steps.get(currentStep).insertStep(latitude, longitude, currentGame.getPosition());
        currentStep++;
        currentMarker.remove();

        if (currentStep > 2) {
            gameRepository.updateGame(steps, firebaseResult -> {
                // Should theoretically be handled
            });
            replay.setValue(true);
        } else {
            startRound();
        }
    }

    public void startRound() {
        gameTimer.stopTimer();
        gameTimer.startTimer(STEP_DURATION, TIMER_UPDATES);

        cityName.postValue(steps.get(currentStep).getCity().getName());
        countryName.postValue(steps.get(currentStep).getCity().getCountry());
    }


    // === INTERFACE IMPLEMENTATIONS

    @Override
    public void onTick(long l) {
        int progress = (int)((l * 100.0) / STEP_DURATION);
        countdown.postValue(progress);
    }

    @Override
    public void onFinish() {
        validateStep();
    }
}
