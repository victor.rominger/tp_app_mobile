package ch.heiafr.tic.mobapp.tp06_duelgeoquiz;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.view.MenuItem;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.databinding.ActivityMainBinding;

/**
 * Single activity class managing the navigation and notification channel.
 */
public class MainActivity extends AppCompatActivity {

    // === ATTRIBUTES

    private NavController navController;
    private ActivityMainBinding binding;


    // === FRAGMENT OVERRIDES

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(binding.toolbar);
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(R.id.destination_splash, R.id.destination_sign_in, R.id.destination_home).build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        initNotificationChan();
    }

    @Override
    public boolean onNavigateUp() {
        return navController.navigateUp();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            navController.navigateUp();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void initNotificationChan() {

        CharSequence name = getString(R.string.channel_name);
        String description = getString(R.string.channel_description);
        NotificationChannel channel = new NotificationChannel(getString(R.string.channel_id), name,
                NotificationManager.IMPORTANCE_DEFAULT);
        channel.setDescription(description);
        ((NotificationManager) getSystemService(NotificationManager.class)).createNotificationChannel(channel);
    }
    // === PUBLIC METHODS

    public void setToolbarVisibility(int visibility) {
        binding.toolbar.setVisibility(visibility);
    }

}