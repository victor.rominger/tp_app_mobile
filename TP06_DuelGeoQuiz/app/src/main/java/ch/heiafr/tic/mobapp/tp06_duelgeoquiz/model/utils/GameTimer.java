package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils;

import android.os.CountDownTimer;

/**
 * Class handling a countdown
 */
public class GameTimer {

    // === SINGLETON

    private static GameTimer instance;

    private GameTimer() {  }

    public static GameTimer getInstance(OnGameTimerEvent listener) {
        if (instance == null) {
            instance = new GameTimer();
        }
        GameTimer.listener = listener;
        return instance;
    }


    // === ATTRIBUTES

    private static OnGameTimerEvent listener;
    private CountDownTimer countDownTimer;


    // === PUBLIC METHODS

    public void startTimer(long countDownTime, long countDownTick) {
        countDownTimer = new CountDownTimer(countDownTime, countDownTick) {
            @Override
            public void onTick(long l) {
                listener.onTick(l);
            }

            @Override
            public void onFinish() {
                listener.onFinish();
            }
        };
        countDownTimer.start();
    }

    public void stopTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }


    // === INTERFACE

    /**
     * Interface to forward countdown events
     */
    public interface OnGameTimerEvent {

        void onTick(long l);

        void onFinish();
    }
}
