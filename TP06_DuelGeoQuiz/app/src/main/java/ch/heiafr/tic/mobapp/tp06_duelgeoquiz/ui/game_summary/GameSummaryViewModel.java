package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.game_summary;

import android.app.Application;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Game;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.GameState;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.GameRepository;

/**
 * ViewModel class linked to the game summary screen.
 */
public class GameSummaryViewModel extends AndroidViewModel {

    // === ATTRIBUTES

    private final Application application;
    private final GameRepository gameRepository;


    // === CONSTRUCTORS

    public GameSummaryViewModel(@NonNull Application application) {
        super(application);
        this.application = application;
        gameRepository = GameRepository.getInstance();
    }


    // === PUBLIC METHODS

    public void play() {
        Game game = gameRepository.getCurrentGame().getValue();
        int round = game.getHalfRound() / 2;
        gameRepository.setCurrentRound(game.getRounds().get(round));
    }

    public boolean canReplay(int round) {
        Game game = gameRepository.getCurrentGame().getValue();
        if (round * 2 <= game.getHalfRound()) {
            gameRepository.setCurrentRound(game.getRounds().get(round - 1));
            return true;
        }
        return false;
    }


    // === GETTERS AND SETTERS

    public LiveData<String> getScore() {
        return Transformations.map(gameRepository.getCurrentGame(), Game::getReadableScore);
    }

    public LiveData<Drawable> getUserAvatar() {
        return Transformations.map(gameRepository.getCurrentGame(), input -> {
            int avatarID = application.getResources().getIdentifier(input.getAvatars().get(input.getPosition()), "drawable", application.getPackageName());
            return ContextCompat.getDrawable(application.getBaseContext(), avatarID);
        });
    }

    public LiveData<String> getUserUsername() {
        return Transformations.map(gameRepository.getCurrentGame(), input -> input.getUsernames().get(input.getPosition()));
    }

    public LiveData<Drawable> getOpponentAvatar() {
        return Transformations.map(gameRepository.getCurrentGame(), input -> {
            int avatarID = application.getResources().getIdentifier(input.getAvatars().get(input.getPosition() ^ 1), "drawable", application.getPackageName());
            return ContextCompat.getDrawable(application.getBaseContext(), avatarID);
        });
    }

    public LiveData<String> getOpponentUsername() {
        return Transformations.map(gameRepository.getCurrentGame(), input -> input.getUsernames().get(input.getPosition() ^ 1));
    }

    public LiveData<Drawable> getUserRoundWinner(int round, int step) {
        return Transformations.map(gameRepository.getCurrentGame(), input -> {
            int winner = input.getRoundWinner(round, step);
            if (round * 2 + 1 >= input.getHalfRound()) {
                return null;
            } else if (input.getPosition() == winner) {
                return ContextCompat.getDrawable(application.getBaseContext(), R.drawable.ic_win);
            } else {
                return ContextCompat.getDrawable(application.getBaseContext(), R.drawable.ic_lose);
            }
        });
    }

    public LiveData<Drawable> getOpponentRoundWinner(int round, int step) {
        return Transformations.map(gameRepository.getCurrentGame(), input -> {
            int winner = input.getRoundWinner(round, step);
            if (round * 2 + 1 >= input.getHalfRound()) {
                return null;
            } else if (input.getPosition() == winner) {
                return application.getDrawable(R.drawable.ic_lose);
            } else {
                return application.getDrawable(R.drawable.ic_win);
            }
        });
    }

    public LiveData<Boolean> isUserTurn() {
        return Transformations.map(gameRepository.getCurrentGame(), input -> input.getStates().get(input.getPosition()) == GameState.PLAYING);
    }

    public LiveData<String> getCountryName(int round) {
        return Transformations.map(gameRepository.getCurrentGame(), input -> {
            if (round * 2 == input.getHalfRound() || round * 2 + 1 == input.getHalfRound()) {
                if (input.getStates().get(input.getPosition()) == GameState.WAITING) {
                    return application.getString(R.string.opponent_turn);
                } else {
                    return application.getString(R.string.your_turn);
                }
            } else if (round * 2 + 1 < input.getHalfRound()) {
                return input.getRounds().get(round).getTitle();
            } else {
                return null;
            }
        });
    }
}
