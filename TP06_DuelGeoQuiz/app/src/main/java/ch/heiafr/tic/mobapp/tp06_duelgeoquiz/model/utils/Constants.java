package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils;

/**
 * Final class holding various constants
 */
public final class Constants {

    public static final String USERS_COLLECTION = "users";
    public static final String GAMES_COLLECTION = "games";
    public static final String UID_FIELD = "uid";
    public static final String UIDS_FIELD = "uids";

    public static final int STEP_DURATION = 30000;
    public static final int TIMER_UPDATES = 100;

    public static final String CITIES_FILE = "cities.json";
    public static final String COUNTRIES_FILE = "countries.json";
    public static final String CONTINENTS_FILE = "continents.json";

    public static final int CITY_THRESHOLD = 3;
    public static final int STEP_COUNT = 3;
    public static final int ROUND_COUNT = 3;

    public static final String JSON_COUNTRY = "country";
    public static final String JSON_NAME = "name";
    public static final String JSON_LATITUDE = "lat";
    public static final String JSON_LONGITUDE = "lng";
    public static final String JSON_CAPITAL = "capital";
    public static final String JSON_CONTINENT = "continent";

}
