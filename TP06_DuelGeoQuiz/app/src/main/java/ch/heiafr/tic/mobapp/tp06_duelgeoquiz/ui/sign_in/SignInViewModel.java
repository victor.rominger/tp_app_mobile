package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.sign_in;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.AuthenticationRepository;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.UserRepository;

/**
 * ViewModel class linked to the sign in screen.
 */
public class SignInViewModel extends AndroidViewModel {

    // === ATTRIBUTES

    private final Application application;
    private final AuthenticationRepository authenticationRepository;
    private final UserRepository userRepository;

    private final MutableLiveData<FirebaseResultState> result = new MutableLiveData<>(null);


    // === CONSTRUCTORS

    public SignInViewModel(@NonNull Application application) {
        super(application);
        this.application = application;

        authenticationRepository = AuthenticationRepository.getInstance();
        userRepository = UserRepository.getInstance();
    }


    // PUBLIC METHODS

    public void signIn(String email, String password, boolean remember) {
        if (email == null || email.isEmpty() || password == null || password.isEmpty()) {
            result.setValue(FirebaseResultState.FAILURE);
        } else {
            result.setValue(FirebaseResultState.LOADING);
            authenticationRepository.signIn(email, password, signInResult -> {
                if (signInResult == FirebaseResultState.SUCCESS) {
                    userRepository.fetchUser(authenticationRepository.getFirebaseUser().getUid(), getUserResult -> {
                        if (getUserResult == FirebaseResultState.SUCCESS) {
                            if (remember) {
                                saveCredentials(email, password);
                            }
                        }
                        result.postValue(getUserResult);
                    });
                } else {
                    result.postValue(signInResult);
                }
            });
        }
    }

    public LiveData<FirebaseResultState> getResult() {
        return result;
    }

    public LiveData<Boolean> isSigningIn() {
        return Transformations.map(result, input -> {
            if (input == null) {
                return false;
            } else {
                return input == FirebaseResultState.LOADING;
            }
        });
    }


    // PRIVATE METHODS

    public void saveCredentials(String email, String password) {
        SharedPreferences sharedPreferences = application.getSharedPreferences(application.getString(R.string.pref_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(application.getString(R.string.pref_remember_email_key), email);
        editor.putString(application.getString(R.string.pref_remember_password_key), password);
        editor.apply();
    }
}
