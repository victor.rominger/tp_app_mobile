package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.game_summary;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.BaseFragment;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.databinding.FragmentGameSummaryBinding;

/**
 * Fragment class managing the game summary screen.
 */
public class GameSummaryFragment extends BaseFragment {

    // === ATTRIBUTES

    private FragmentGameSummaryBinding binding;


    // === FRAGMENT OVERRIDES

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_game_summary, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbarVisibility = View.VISIBLE;

        GameSummaryViewModel viewModel = new ViewModelProvider(this).get(GameSummaryViewModel.class);

        NavController navController = NavHostFragment.findNavController(this);

        binding.buttonPlay.setOnClickListener(v -> {
            viewModel.play();
            GameSummaryFragmentDirections.ActionDestinationGameSummaryToDestinationGameMaps action =
                    GameSummaryFragmentDirections.actionDestinationGameSummaryToDestinationGameMaps(false);
            navController.navigate(action);
        });

        binding.cardRound1.setOnClickListener(v -> {
            if (viewModel.canReplay(1)) {
                GameSummaryFragmentDirections.ActionDestinationGameSummaryToDestinationGameMaps action =
                        GameSummaryFragmentDirections.actionDestinationGameSummaryToDestinationGameMaps(true);
                navController.navigate(action);
            } else {
                Toast.makeText(getContext(), getString(R.string.toast_not_available_yet), Toast.LENGTH_SHORT).show();
            }
        });

        binding.cardRound2.setOnClickListener(v -> {
            if (viewModel.canReplay(2)) {
                GameSummaryFragmentDirections.ActionDestinationGameSummaryToDestinationGameMaps action =
                        GameSummaryFragmentDirections.actionDestinationGameSummaryToDestinationGameMaps(true);
                navController.navigate(action);
            } else {
                Toast.makeText(getContext(), getString(R.string.toast_not_available_yet), Toast.LENGTH_SHORT).show();
            }
        });

        binding.cardRound3.setOnClickListener(v -> {
            if (viewModel.canReplay(3)) {
                GameSummaryFragmentDirections.ActionDestinationGameSummaryToDestinationGameMaps action =
                        GameSummaryFragmentDirections.actionDestinationGameSummaryToDestinationGameMaps(true);
                navController.navigate(action);
            } else {
                Toast.makeText(getContext(), getString(R.string.toast_not_available_yet), Toast.LENGTH_SHORT).show();
            }
        });

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(getViewLifecycleOwner());
    }
}