package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums;

/**
 * Enumeration for the results of a Firebase action
 */
public enum FirebaseResultState {
    IDLE,
    SUCCESS,
    FAILURE,
    LOADING,
}
