package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data;

/**
 * Data class to hold information about a continent.
 */
public class Continent {

    private final String code;
    private final String name;

    public Continent(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
