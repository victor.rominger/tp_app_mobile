package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;

/**
 * Interface used as a callback for repositories actions
 */
public interface OnRepositoryEvent {
    void onFirebaseResult(FirebaseResultState firebaseResultState);
}
