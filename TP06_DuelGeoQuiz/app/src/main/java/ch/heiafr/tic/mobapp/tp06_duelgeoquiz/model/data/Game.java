package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data;

import com.google.firebase.firestore.Exclude;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.GameState;

/**
 * Data class to hold information about a game.
 */
public class Game {

    // === ATTRIBUTES

    private String gid;                     // id of the game
    private int halfRound;                  // current round when counting for each player
    private List<Round> rounds;             // rounds

    private List<Integer> scores;           // scores for each player
    private List<String> uids;              // uids for each player
    private List<String> avatars;           // avatars for each player
    private List<String> usernames;         // usernames for each player
    private List<GameState> states;         // states for each player

    @Exclude private int position;          // index in the lists for the current player


    // === CONSTRUCTORS

    public Game() {  }

    public Game(String gid, User user, User opponent, List<Round> rounds) {
        this.gid = gid;
        this.rounds = rounds;
        halfRound = 0;

        scores = new ArrayList<>(Arrays.asList(0, 0));
        uids = new ArrayList<>(Arrays.asList(user.getUid(), opponent.getUid()));
        avatars = new ArrayList<>(Arrays.asList(user.getAvatar(), opponent.getAvatar()));
        usernames = new ArrayList<>(Arrays.asList(user.getUsername(), opponent.getUsername()));
        states = new ArrayList<>(Arrays.asList(GameState.PLAYING, GameState.WAITING));
    }


    // === GETTERS AND SETTERS

    public String getGid() {
        return gid;
    }

    public int getHalfRound() {
        return halfRound;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public List<Integer> getScores() {
        return scores;
    }

    public List<String> getUids() {
        return uids;
    }

    public List<String> getAvatars() {
        return avatars;
    }

    public List<String> getUsernames() {
        return usernames;
    }

    public List<GameState> getStates() {
        return states;
    }

    @Exclude public int getPosition() {
        return position;
    }

    @Exclude public void setPosition(int position) {
        this.position = position;
    }


    // === PUBLIC METHODS

    @Exclude public String getReadableScore() {
        return scores.get(position) + "-" + scores.get(position ^ 1);
    }

    @Exclude public int getRoundWinner(int round, int step) {
        return rounds.get(round).getStepWinner(step, position);
    }

    @Exclude public void updateUser(User user) {
        avatars.set(position, user.getAvatar());
        usernames.set(position, user.getUsername());
    }

    @Exclude public void endRound(List<Step> steps) {
        Round round = rounds.get(halfRound / 2);
        round.setSteps(steps);

        halfRound++;
        scores.set(position, calculateScore(position));
        scores.set(position ^ 1, calculateScore(position ^ 1));

        if (halfRound > 5) {
            if (scores.get(position) > scores.get(position ^ 1)) {
                states.set(position, GameState.WON);
                states.set(position ^ 1, GameState.LOST);
            } else {
                states.set(position, GameState.LOST);
                states.set(position ^ 1, GameState.WON);
            }
        } else {
            Collections.swap(states, 0, 1);
        }
    }

    @Exclude public int calculateScore(int position) {
        int score = 0;
        for(int i = 0; i < halfRound / 2; i++) {
            for (int j = 0; j < rounds.get(i).getSteps().size(); j++) {
                if (position == rounds.get(i).getStepWinner(j, position)) {
                    score++;
                }
            }
        }
        return score;
    }
}
