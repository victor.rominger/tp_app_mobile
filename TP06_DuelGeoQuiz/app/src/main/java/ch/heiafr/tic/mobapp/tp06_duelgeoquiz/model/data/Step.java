package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data;

import com.google.firebase.firestore.Exclude;

import java.util.ArrayList;
import java.util.List;

/**
 * Data class to hold information about a step
 */
public class Step {

    // === ATTRIBUTES

    private City city;

    private List<Double> latitudes;
    private List<Double> longitudes;


    // === CONSTRUCTORS

    public Step() {}

    public Step(City city) {
        this.city = city;

        latitudes = new ArrayList<>();          // latitudes for each player
        longitudes = new ArrayList<>();         // longitudes for each player
    }


    // === GETTERS AND SETTERS

    public City getCity() {
        return city;
    }

    public List<Double> getLatitudes() {
        return latitudes;
    }

    public List<Double> getLongitudes() {
        return longitudes;
    }


    // === PUBLIC METHODS

    @Exclude public void insertStep(double latitude, double longitude, int position) {
        latitudes.add(position, latitude);
        longitudes.add(position, longitude);
    }

    // https://stackoverflow.com/questions/8049612/calculating-distance-between-two-geographic-locations
    @Exclude public double distanceBetween(int user) {
        if (latitudes == null || latitudes.size() <= user) {
            return 0.0;
        }

        float pk = (float) (180.f/Math.PI);

        double a1 = city.getLatitude() / pk;
        double a2 = city.getLongitude() / pk;
        double b1 = latitudes.get(user) / pk;
        double b2 = longitudes.get(user) / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return 6366000 * tt;
    }
}
