package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.settings;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.User;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.AuthenticationRepository;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.UserRepository;

/**
 * ViewModel class linked to the settings screen.
 */
public class SettingsViewModel extends AndroidViewModel {

    // === ATTRIBUTES

    private final Application application;
    private final AuthenticationRepository authenticationRepository;
    private final UserRepository userRepository;

    private final MutableLiveData<Boolean> saving = new MutableLiveData<>(false);
    private final MutableLiveData<Integer> avatarIndex = new MutableLiveData<>(0);
    private final MutableLiveData<FirebaseResultState> result = new MutableLiveData<>(null);


    // === CONSTRUCTORS

    public SettingsViewModel(@NonNull Application application) {
        super(application);
        this.application = getApplication();
        authenticationRepository = AuthenticationRepository.getInstance();
        userRepository = UserRepository.getInstance();

        String[] avatars = application.getResources().getStringArray(R.array.avatar_values);
        avatarIndex.setValue(Arrays.asList(avatars).indexOf(userRepository.getLoggedUser().getAvatar()));
    }


    // GETTERS AND SETTERS

    public User getUser() {
        return userRepository.getLoggedUser();
    }

    public FirebaseUser getFirebaseUser() {
        return authenticationRepository.getFirebaseUser();
    }

    public LiveData<Boolean> isSaving() {
        return saving;
    }

    public LiveData<FirebaseResultState> getResult() {
        return result;
    }


    // PUBLIC METHODS

    public void signOut() {
        authenticationRepository.signOut();

        SharedPreferences sharedPreferences = application.getSharedPreferences(application.getString(R.string.pref_file_key), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(application.getString(R.string.pref_remember_email_key), null);
        editor.putString(application.getString(R.string.pref_remember_password_key), null);
        editor.apply();
    }

    public void saveSettings(String username, String title, String email) {
        saving.setValue(true);

        User user = userRepository.getLoggedUser();
        String avatar = application.getResources().getStringArray(R.array.avatar_values)[avatarIndex.getValue()];
        if (!username.equals(user.getUsername()) || !title.equals(user.getAvatar()) || !avatar.equals(user.getAvatar())) {
            user.setUsername(username);
            user.setTitle(title);
            user.setAvatar(avatar);
            updateUser(user);
        }

        FirebaseUser firebaseUser = authenticationRepository.getFirebaseUser();
        if (!email.equals(firebaseUser.getEmail())) {
            updateEmail(email);
        }
    }

    public void updateUser(User user) {
        userRepository.updateUser(user, firebaseResult -> {
            result.postValue(firebaseResult);
            saving.postValue(false);
        });
    }

    public void updateEmail(String email) {
        authenticationRepository.updateEmail(email, firebaseResult -> {
            result.postValue(firebaseResult);
            saving.postValue(false);
        });
    }

    public LiveData<Drawable> getColor(int position) {
        return Transformations.map(avatarIndex, input -> {
            if (position == input) {
                return new ColorDrawable(application.getResources().getColor(R.color.purple_500));
            } else {
                return null;
            }
        });
    }

    public void onAvatarClicked(int position) {
        avatarIndex.postValue(position);
    }
}
