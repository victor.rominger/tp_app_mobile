package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data;

/**
 * Data class to hold information about a city.
 */
public class City {

    // === ATTRIBUTES

    private String name;
    private String country;
    private double latitude;
    private double longitude;


    // === CONSTRUCTORS

    public City() { }

    public City(String name, double latitude, double longitude) {
        this.name = name;
        this.latitude = latitude;
        this.longitude = longitude;
    }


    // === GETTERS AND SETTERS

    public String getName() {
        return name;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
