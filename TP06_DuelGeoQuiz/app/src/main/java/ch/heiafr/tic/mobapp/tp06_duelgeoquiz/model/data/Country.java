package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data;

/**
 * Data class to hold information about a country.
 */
public class Country {

    // === ATTRIBUTES

    private final String code;        // abbreviation (e.g. "CH" for Switzerland)
    private final String name;
    private final City capital;


    // === CONSTRUCTORS

    public Country(String code, String name, City capital) {
        this.code = code;
        this.name = name;
        this.capital = capital;
    }


    // === GETTERS AND SETTERS

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public City getCapital() {
        return capital;
    }
}
