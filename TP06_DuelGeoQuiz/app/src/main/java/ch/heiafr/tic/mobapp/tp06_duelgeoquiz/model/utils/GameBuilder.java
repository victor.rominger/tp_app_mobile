package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.City;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Continent;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Country;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Game;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Round;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Step;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.User;

import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.CITY_THRESHOLD;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.JSON_CAPITAL;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.JSON_CONTINENT;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.JSON_COUNTRY;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.JSON_LATITUDE;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.JSON_LONGITUDE;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.JSON_NAME;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.ROUND_COUNT;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.STEP_COUNT;

/**
 * Helper class allowing the read the json assets (cities and countries) and creating a game.
 */
public class GameBuilder {

    // === ATTRIBUTES

    private static final List<Continent> continents = new ArrayList<>();
    private static final List<Country> countries = new ArrayList<>();
    private static final Map<String, List<City>> citiesPerCountry = new TreeMap<>();
    private static final Map<String, List<Country>> countriesPerContinent = new TreeMap<>();
    private static final Random random = new Random();


    // === PUBLIC METHODS

    public static void loadData(InputStream streamContinent, InputStream streamCountry, InputStream streamCity) {
        if (continents.size() == 0) {
            loadContinents(streamContinent);
        }

        if (countries.size() == 0) {
            loadCountries(streamCountry);
        }

        if (citiesPerCountry.size() == 0) {
            loadCities(streamCity);
        }
    }

    public static Game buildGame(String gid, User user, User opponent, boolean capitalOnly) {
        if (capitalOnly) {
            List<Integer> indexContinents = new ArrayList<>();
            while (indexContinents.size() < ROUND_COUNT) {
                int index = random.nextInt(continents.size());
                if (!indexContinents.contains(index)) {
                    indexContinents.add(index);
                }
            }

            List<Round> rounds = new ArrayList<>();
            for (int i = 0; i < ROUND_COUNT; i++) {
                Continent continent = continents.get((Integer) indexContinents.get(i));
                List<Country> countries = countriesPerContinent.get(continent.getCode());

                List<Integer> indexCountries = new ArrayList<>();
                while (indexCountries.size() < STEP_COUNT) {
                    int index = random.nextInt(countries.size());
                    if (!indexCountries.contains(index)) {
                        indexCountries.add(index);
                    }
                }

                List<Step> steps = new ArrayList<>();
                for (int j = 0; j < STEP_COUNT; j++) {
                    City city = countries.get(indexCountries.get(j)).getCapital();
                    steps.add(new Step(city));
                }

                rounds.add(new Round(continent.getName(), steps));
            }

            return new Game(gid, user, opponent, rounds);
        } else {
            List<Integer> indexCountries = new ArrayList<>();
            while (indexCountries.size() < ROUND_COUNT) {
                int index = random.nextInt(countries.size());
                if (!indexCountries.contains(index) && citiesPerCountry.get(countries.get(index).getCode()).size() >= 3) {
                    indexCountries.add(index);
                }
            }

            List<Round> rounds = new ArrayList<>();
            for (int i = 0; i < ROUND_COUNT; i++) {
                Country country = countries.get((Integer) indexCountries.get(i));
                List<City> cities = citiesPerCountry.get(country.getCode());

                List<Integer> indexCities = new ArrayList<>();
                while (indexCities.size() < STEP_COUNT) {
                    int index = random.nextInt(cities.size());
                    if (!indexCities.contains(index)) {
                        indexCities.add(index);
                    }
                }

                List<Step> steps = new ArrayList<>();
                for (int j = 0; j < STEP_COUNT; j++) {
                    City city = cities.get(indexCities.get(j));
                    city.setCountry(country.getName());
                    steps.add(new Step(city));
                }

                rounds.add(new Round(country.getName(), steps));
            }

            return new Game(gid, user, opponent, rounds);
        }
    }


    // === PRIVATE METHODS

    private static void loadContinents(InputStream inputStream) {
        try {
            int size = inputStream.available();
            byte[] buffer = new byte[size];

            inputStream.read(buffer);
            inputStream.close();
            String json = new String(buffer);

            JSONObject jsonObj = new JSONObject(json);

            for (Iterator<String> it = jsonObj.keys(); it.hasNext(); ) {
                String key = it.next();
                String name = jsonObj.getString(key);
                continents.add(new Continent(key, name));
            }

        } catch (Exception ignored) {

        }
    }

    private static void loadCountries(InputStream inputStream) {
        try {
            int size = inputStream.available();
            byte[] buffer = new byte[size];

            inputStream.read(buffer);
            inputStream.close();
            String json = new String(buffer);

            JSONObject jsonObj = new JSONObject(json);

            for (Continent continent : continents) {
                countriesPerContinent.put(continent.getCode(), new ArrayList<>());
            }

            for (Iterator<String> it = jsonObj.keys(); it.hasNext(); ) {
                String key = it.next();
                JSONObject jsonCountry = jsonObj.getJSONObject(key);
                String continent = jsonCountry.getString(JSON_CONTINENT);
                String name = jsonCountry.getString(JSON_NAME);
                JSONObject jsonCity = jsonCountry.getJSONObject(JSON_CAPITAL);
                String city = jsonCity.getString(JSON_NAME);
                double latitude = jsonCity.getDouble(JSON_LATITUDE);
                double longitude = jsonCity.getDouble(JSON_LONGITUDE);
                City capital = new City(city, latitude, longitude);
                capital.setCountry(name);
                Country country = new Country(key, name, capital);
                countries.add(country);
                countriesPerContinent.get(continent).add(country);
            }

        } catch (Exception ignored) {

        }
    }

    private static void loadCities(InputStream inputStream) {
        try {
            int size = inputStream.available();
            byte[] buffer = new byte[size];

            inputStream.read(buffer);
            inputStream.close();
            String json = new String(buffer);

            JSONArray jsonCities = new JSONArray(json);
            List<City> cities = new ArrayList<>();

            String previousCode = jsonCities.getJSONObject(0).getString(JSON_COUNTRY);

            for (int i = 0; i < jsonCities.length(); i++) {
                JSONObject jsonCity = jsonCities.getJSONObject(i);
                String countryCode = jsonCity.getString(JSON_COUNTRY);
                String name = jsonCity.getString(JSON_NAME);
                double latitude = jsonCity.getDouble(JSON_LATITUDE);
                double longitude = jsonCity.getDouble(JSON_LONGITUDE);

                if (!countryCode.equals(previousCode)) {
                    if (cities.size() >= CITY_THRESHOLD) {
                        citiesPerCountry.put(previousCode, new ArrayList<>(cities));
                    }
                    cities.clear();
                    previousCode = countryCode;
                }
                cities.add(new City(name, latitude, longitude));

            }

        } catch (Exception ignored) {

        }
    }
}
