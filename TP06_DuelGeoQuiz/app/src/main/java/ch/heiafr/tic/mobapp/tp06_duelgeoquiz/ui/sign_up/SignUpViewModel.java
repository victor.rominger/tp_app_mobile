package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.sign_up;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.AuthenticationRepository;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.UserRepository;

/**
 * ViewModel class linked to the sign up screen.
 */
public class SignUpViewModel extends AndroidViewModel {

    // === ATTRIBUTES

    private final Application application;
    private final AuthenticationRepository authenticationRepository;
    private final UserRepository userRepository;

    private final MutableLiveData<FirebaseResultState> result = new MutableLiveData<>(null);


    // === CONSTRUCTORS

    public SignUpViewModel(@NonNull Application application) {
        super(application);
        this.application = application;

        authenticationRepository = AuthenticationRepository.getInstance();
        userRepository = UserRepository.getInstance();
    }


    // === PUBLIC METHODS

    public void signUp(String username, String email, String password) {
        if (username == null || username.isEmpty() || email == null || email.isEmpty() || password == null || password.isEmpty()) {
            result.setValue(FirebaseResultState.FAILURE);
        } else {
            result.setValue(FirebaseResultState.LOADING);
            authenticationRepository.signUp(email, password, signUpResultState -> {
                if (signUpResultState == FirebaseResultState.SUCCESS) {
                    userRepository.createUser(
                            authenticationRepository.getFirebaseUser().getUid(),
                            username,
                            application.getString(R.string.rookie),
                            result::postValue);
                } else {
                    result.postValue(signUpResultState);
                }
            });
        }
    }

    public LiveData<FirebaseResultState> getResult() {
        return result;
    }

    public LiveData<Boolean> isSigningUp() {
        return Transformations.map(result, input -> {
            if (input == null) {
                return false;
            } else {
                return input == FirebaseResultState.LOADING;
            }
        });
    }
}
