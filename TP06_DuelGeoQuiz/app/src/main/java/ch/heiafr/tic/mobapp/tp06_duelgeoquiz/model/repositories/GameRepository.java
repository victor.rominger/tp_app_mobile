package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Game;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Round;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Step;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.User;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.GameBuilder;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.OnRepositoryEvent;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.GameState;

import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.GAMES_COLLECTION;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.UIDS_FIELD;

/**
 * Repository class that manages the game logic and holds the data.
 */
public class GameRepository {

    // === SINGLETON

    private GameRepository() {
        setupRealTimeListeners();
    }

    public static GameRepository getInstance() {
        if (instance == null) {
            instance = new GameRepository();
        }
        return instance;
    }

    // === ATTRIBUTES
    private static volatile GameRepository instance;
    private final MutableLiveData<Game> currentGame = new MutableLiveData(null);
    private final MutableLiveData<Round> currentRound = new MutableLiveData(null);
    private final FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
    private final MutableLiveData<List<Game>> games = new MutableLiveData(null);
    private final MutableLiveData<FirebaseResultState> notification = new MutableLiveData(null);


    // === GETTERS AND SETTERS

    public LiveData<List<Game>> getGames() {
        return games;
    }

    public LiveData<FirebaseResultState> getNotification() {
        return notification;
    }

    public LiveData<Game> getCurrentGame() {
        return currentGame;
    }

    public void setCurrentGame(Game game) {
        currentGame.setValue(game);
    }

    public LiveData<Round> getCurrentRound() {
        return currentRound;
    }

    public void setCurrentRound(Round round) {
        currentRound.setValue(round);
    }


    // === PUBLIC METHODS

    // Creates a new game
    public void createGame(User user, User opponent, boolean capitalOnly, OnRepositoryEvent callback) {
        DocumentReference documentReference = this.firebaseFirestore.collection(GAMES_COLLECTION)
                .document();
        Game game = GameBuilder.buildGame(documentReference.getId(), user, opponent, capitalOnly);
        this.currentGame.setValue(game);
        documentReference.set(game).addOnCompleteListener(
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            callback.onFirebaseResult(FirebaseResultState.SUCCESS);
                        } else {
                            callback.onFirebaseResult(FirebaseResultState.FAILURE);
                        }
                    }
                }
        );
    }

    // Fetch all games where the user is a player
    public void fetchGames(OnRepositoryEvent callback) {
        User user = UserRepository.getInstance().getLoggedUser();
        this.firebaseFirestore.collection(GAMES_COLLECTION)
                .whereArrayContains(UIDS_FIELD, user.getUid())
                .get()
                .addOnCompleteListener(
                        new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull @NotNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    List<Game> newGames = new ArrayList<>();
                                    for (QueryDocumentSnapshot queryDocumentSnapshot :
                                            (QuerySnapshot) Objects.requireNonNull(task.getResult())
                                    ) {
                                        Game newGame = (Game) queryDocumentSnapshot.toObject(Game.class);
                                        newGame.setPosition(newGame.getUids().indexOf(user.getUid()));
                                        newGames.add(newGame);
                                    }
                                    games.postValue(newGames);
                                    callback.onFirebaseResult(FirebaseResultState.SUCCESS);
                                    return;
                                }
                                callback.onFirebaseResult(FirebaseResultState.FAILURE);
                            }
                        }
                );
    }

    // Update a game
    public void updateGame(List<Step> steps, OnRepositoryEvent callback) {
        Game game = currentGame.getValue();
        game.endRound(steps);
        setCurrentGame(game);
        this.firebaseFirestore.collection(GAMES_COLLECTION)
                .document(game.getGid())
                .set(game)
                .addOnCompleteListener(
                        new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull @NotNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    callback.onFirebaseResult(FirebaseResultState.SUCCESS);
                                } else {
                                    callback.onFirebaseResult(FirebaseResultState.FAILURE);
                                }
                            }
                        }
                );
    }


    // === PRIVATE METHODS

    // Enable listeners to allow the real time feature
    private void setupRealTimeListeners() {
        User user = UserRepository.getInstance().getLoggedUser();
        this.firebaseFirestore.collection(GAMES_COLLECTION).addSnapshotListener(
                new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable @org.jetbrains.annotations.Nullable QuerySnapshot value, @Nullable @org.jetbrains.annotations.Nullable FirebaseFirestoreException error) {
                        if (error == null) {
                            notification.postValue(FirebaseResultState.LOADING);
                            List<Game> newGames = new ArrayList<>();
                            assert value != null;
                            for (QueryDocumentSnapshot queryDocumentSnapshot : value) {
                                Game newGame = (Game) queryDocumentSnapshot.toObject(Game.class);
                                int position = newGame.getUids().indexOf(user.getUid());
                                if (position != -1) {
                                    newGame.setPosition(position);
                                    newGames.add(newGame);
                                    if (currentGame.getValue() != null && currentGame.getValue().getGid().equals(newGame.getGid())) {
                                        currentGame.setValue(newGame);
                                    }
                                }
                            }
                            List<Game> oldGames = games.getValue();
                            if (oldGames != null) {
                                for (Game nG : newGames) {
                                    if (nG.getStates().get(nG.getPosition()) == GameState.PLAYING) {
                                        for (Game og : oldGames) {
                                            if (og.getGid().equals(nG.getGid()) && og.getStates().get(og.getPosition()) == GameState.WAITING) {
                                                notification.postValue(FirebaseResultState.SUCCESS);
                                                games.postValue(newGames);
                                                return;
                                            }
                                        }
                                        continue;
                                    }
                                }
                            }
                            games.postValue(newGames);
                            notification.postValue(FirebaseResultState.FAILURE);
                        }
                    }
                }
        );
    }
}
