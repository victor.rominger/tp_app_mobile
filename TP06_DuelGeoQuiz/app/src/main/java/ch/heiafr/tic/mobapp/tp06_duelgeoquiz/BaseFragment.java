package ch.heiafr.tic.mobapp.tp06_duelgeoquiz;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

/**
 * Base fragment class extended by all to fragments in the application which allow them to
 * change the visibility of the toolbar.
 */
public abstract class BaseFragment extends Fragment {

    // === ATTRIBUTES

    private MainActivity mainActivity;
    protected int toolbarVisibility = View.VISIBLE;


    // === FRAGMENT OVERRIDES

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (getActivity() instanceof MainActivity) {
            mainActivity = (MainActivity)getActivity();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mainActivity.setToolbarVisibility(toolbarVisibility);
    }


    // PROTECTED METHODS

    protected void setToolbarVisibility(int visibility) {
        toolbarVisibility = visibility;
        mainActivity.setToolbarVisibility(toolbarVisibility);
    }
}
