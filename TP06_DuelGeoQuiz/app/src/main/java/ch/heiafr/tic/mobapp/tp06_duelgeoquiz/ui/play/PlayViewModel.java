package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.play;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import java.util.ArrayList;
import java.util.List;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.User;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.GameRepository;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.UserRepository;

/**
 * ViewModel class linked to the players screen where the user can start a games.
 */
public class PlayViewModel extends AndroidViewModel {

    // === ATTRIBUTES

    private final GameRepository gameRepository;
    private final UserRepository userRepository;

    private final MutableLiveData<FirebaseResultState> result = new MutableLiveData<>(null);
    private final MutableLiveData<List<User>> users = new MutableLiveData<>(null);


    // === CONSTRUCTORS

    public PlayViewModel(@NonNull Application application) {
        super(application);

        gameRepository = GameRepository.getInstance();
        userRepository = UserRepository.getInstance();

        fetchUsers();
    }


    // === GETTERS AND SETTERS

    public LiveData<List<User>> getUserList() {
        return Transformations.map(userRepository.getUsers(), input -> {
            users.setValue(input);
            if (input != null) {
                return input;
            } else {
                return new ArrayList<>();
            }
        });
    }

    public LiveData<FirebaseResultState> getResult() {
        return result;
    }

    public LiveData<List<User>> getUsers() {
        return users;
    }


    // === PUBLIC METHODS

    private void fetchUsers() {
        result.setValue(FirebaseResultState.LOADING);
        userRepository.fetchUsers(result::postValue);
    }

    public void createGame(User opponent, boolean capitalOnly) {
        gameRepository.createGame(userRepository.getLoggedUser(), opponent, capitalOnly, result::postValue);
    }
}
