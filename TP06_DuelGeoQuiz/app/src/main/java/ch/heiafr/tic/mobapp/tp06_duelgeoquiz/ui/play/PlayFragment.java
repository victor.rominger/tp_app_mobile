package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.play;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.BaseFragment;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.databinding.FragmentPlayBinding;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.User;

/**
 * Fragment class managing the players screen where the user can start a game.
 */
public class PlayFragment extends BaseFragment {

    // === ATTRIBUTES

    private FragmentPlayBinding binding;


    // === CONSTRUCTORS

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_play, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbarVisibility = View.VISIBLE;

        PlayViewModel viewModel = new ViewModelProvider(this).get(PlayViewModel.class);

        PlayAdapter adapter = new PlayAdapter(new ArrayList<>(), viewModel, user -> {
            AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
            builder.setTitle(R.string.choose_mode)
                    .setItems(R.array.game_modes, (dialog, which) -> {
                        viewModel.createGame(user, which == 0);
                        NavController navController = NavHostFragment.findNavController(this);
                        navController.navigate(R.id.action_destination_play_to_destination_game_summary);
                    });
            builder.create().show();
        });

        viewModel.getUserList().observe(getViewLifecycleOwner(), adapter::updateList);

        viewModel.getUserList().observe(getViewLifecycleOwner(), players -> {
            updateUI(viewModel.getResult().getValue(), players);
            adapter.updateList(players);
        });

        viewModel.getResult().observe(getViewLifecycleOwner(), firebaseResult -> updateUI(firebaseResult, viewModel.getUsers().getValue()));

        binding.listPlay.setAdapter(adapter);
        binding.listPlay.setLayoutManager(new LinearLayoutManager(getContext()));
    }


    // === PRIVATE METHODS

    private void updateUI(FirebaseResultState firebaseResult, List<User> games) {
        if (firebaseResult == null || games == null) {
            return;
        }

        if (firebaseResult == FirebaseResultState.SUCCESS) {
            if (games.size() > 0) {
                binding.listPlay.setVisibility(View.VISIBLE);
                binding.groupNoUser.setVisibility(View.INVISIBLE);
            } else {
                binding.listPlay.setVisibility(View.INVISIBLE);
                binding.groupNoUser.setVisibility(View.VISIBLE);
            }
            binding.progressPlay.setVisibility(View.INVISIBLE);
        } else if (firebaseResult == FirebaseResultState.FAILURE) {
            Toast.makeText(getContext(), R.string.toast_error_game, Toast.LENGTH_SHORT).show();
        }
    }
}