package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data;

/**
 * Data class to hold information about a user
 */
public class User {

    // === ATTRIBUTES

    private String uid;
    private String username;
    private String title;
    private String avatar;


    // === CONSTRUCTORS

    public User() { }

    public User(String uid, String username, String title) {
        this.uid = uid;
        this.username = username;
        this.title = title;
        avatar = "ic_avatar_1";
    }


    // === GETTERS AND SETTERS

    public String getUid() {
        return uid;
    }

    public String getUsername() {
        return username;
    }

    public String getAvatar() {
        return avatar;
    }

    public String getTitle() {
        return title;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }
}
