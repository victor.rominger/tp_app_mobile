package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.game_maps;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import android.app.Application;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.jetbrains.annotations.NotNull;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.BaseFragment;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.databinding.FragmentGameMapsBinding;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Step;

/**
 * Fragment class managing the game screen including the Google Maps.
 */
public class GameMapsFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    // === ATTRIBUTES

    private GoogleMap map;
    private FragmentGameMapsBinding binding;
    private GameMapsViewModel viewModel;


    // === FRAGMENT OVERRIDES

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_game_maps, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        GameMapsFragmentArgs args = GameMapsFragmentArgs.fromBundle(getArguments());

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        GameMapsViewModelProvider gameMapsViewModelProvider = new GameMapsViewModelProvider(getActivity().getApplication(), args.getReplay());
        viewModel = new ViewModelProvider(this, gameMapsViewModelProvider).get(GameMapsViewModel.class);

        binding.buttonValidate.setOnClickListener(v -> viewModel.validateStep());

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
        viewModel.isReplay().observe(getViewLifecycleOwner(), (Observer) replay -> {
            if ((Boolean) replay) {
                mapFragment.getMapAsync(this);
            }
        });
    }

    // === PRIVATE METHODS

    private void drawMap() {
        if(viewModel.isReplay().getValue()) {
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                boolean success = map.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                this.getContext(), R.raw.default_map_settings));

                if (!success) {
                    Log.e("Error", "Style parsing failed.");
                }
            } catch (Resources.NotFoundException e) {
                Log.e("Error", "Can't find style. Error: ", e);
            }
            setToolbarVisibility(View.VISIBLE);
            binding.groupMaps.setVisibility(View.VISIBLE);
            binding.cardGameMaps.setVisibility(View.INVISIBLE);
            BitmapDescriptor green = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
            BitmapDescriptor red = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED);
            BitmapDescriptor blue = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE);
            for (Step step : viewModel.getCurrentRound().getSteps()) {
                LatLng correctPosition = new LatLng(step.getCity().getLatitude(), step.getCity().getLongitude());
                map.addMarker(new MarkerOptions()
                        .icon(green)
                        .position(correctPosition)
                );
                for (int i = 0; i < step.getLatitudes().size(); i++) {
                    LatLng userPosition = new LatLng(step.getLatitudes().get(i), step.getLongitudes().get(i));
                    map.addMarker(new MarkerOptions()
                            .icon(viewModel.getCurrentGame().getPosition() == i ? blue : red)
                            .position(userPosition)
                    );
                    map.addPolyline(new PolylineOptions().add(correctPosition, userPosition)
                            .color(Color.BLACK)
                            .width(5));

                }
            }
            map.setOnMapClickListener(null);
        } else {
            setToolbarVisibility(View.GONE);
            binding.groupMaps.setVisibility(View.VISIBLE);
            try {
                // Customise the styling of the base map using a JSON object defined
                // in a raw resource file.
                boolean success = map.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                this.getContext(), R.raw.map_settings));

                if (!success) {
                    Log.e("Error", "Style parsing failed.");
                }
            } catch (Resources.NotFoundException e) {
                Log.e("Error", "Can't find style. Error: ", e);
            }
            map.setOnMapClickListener(this);
            // Add a marker in Sydney and move the camera
            LatLng position = new LatLng(49, 16);
            viewModel.setCurrentMarker(map.addMarker(new MarkerOptions()
                    .position(position)
                    .title("Initial position")));
            map.moveCamera(CameraUpdateFactory.newLatLng(position));
            binding.textCity.setText(viewModel.getCityName().getValue());
            binding.textCountry.setText(viewModel.getCountryName().getValue());
            viewModel.startRound();
        }
    }


    // === INTERFACE IMPLEMENTATIONS

    @Override
    public void onMapReady(@NonNull @NotNull GoogleMap googleMap) {
        map = googleMap;
        binding.progressMapsLoading.setVisibility(View.INVISIBLE);
        drawMap();
    }

    @Override
    public void onMapClick(@NonNull @NotNull LatLng latLng) {
        viewModel.getCurrentMarker().remove();
        viewModel.setCurrentMarker(map.addMarker(new MarkerOptions()
                .position(latLng)
                .title("Selected position")));
    }

    // === VIEWMODEL PROVIDERS

    /**
     * View model provider factory to instantiate a view model with a custom constructor.
     */
    public static class GameMapsViewModelProvider implements ViewModelProvider.Factory {

        private final Application application;
        private final boolean replay;

        public GameMapsViewModelProvider(Application application, boolean replay) {
            this.application = application;
            this.replay = replay;
        }

        @NonNull
        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            if (modelClass.isAssignableFrom(GameMapsViewModel.class)) {
                return (T) new GameMapsViewModel(application, replay);
            }
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }
}