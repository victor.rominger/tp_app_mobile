package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.databinding.ItemGameBinding;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Game;

/**
 * Recycler adapter class managing the list of games.
 */
public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.GameViewHolder> {

    // === ATTRIBUTES

    private List<Game> items;
    private final OnGameItemInteraction callback;


    // === CONSTRUCTORS

    public HomeAdapter(List<Game> items, OnGameItemInteraction callback) {
        this.items = items;
        this.callback = callback;
    }


    // === ADAPTER OVERRIDES

    @NonNull
    @Override
    public GameViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemGameBinding gameBinding = ItemGameBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new GameViewHolder(gameBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull GameViewHolder holder, int position) {
        holder.bind(items.get(position), callback);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    // === PUBLIC METHODS

    public void updateList(List<Game> items) {
        this.items = items;
        notifyDataSetChanged();
    }


    // === VIEW HOLDERS

    /**
     * View holder class managing a game cell
     */
    public static class GameViewHolder extends RecyclerView.ViewHolder {

        private final ItemGameBinding binding;

        public GameViewHolder(ItemGameBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(Game game, OnGameItemInteraction callback) {
            int opponent = game.getPosition() ^ 1;

            Context context = itemView.getContext();
            int avatarID = context.getResources().getIdentifier(game.getAvatars().get(opponent), "drawable", context.getPackageName());

            binding.textGameUsername.setText(game.getUsernames().get(opponent));
            binding.imageGameAvatar.setImageResource(avatarID);

            switch (game.getStates().get(game.getPosition())) {
                case LOST:
                    binding.imageGameState.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_lose));
                    binding.textGameRound.setText(context.getString(R.string.game_over));
                    break;
                case PLAYING:
                    binding.imageGameState.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_playing));
                    binding.textGameRound.setText(context.getString(R.string.current_round, game.getHalfRound() / 2 + 1));
                    break;
                case WAITING:
                    binding.imageGameState.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_waiting));
                    binding.textGameRound.setText(context.getString(R.string.current_round, game.getHalfRound() / 2 + 1));
                    break;
                default:
                    binding.imageGameState.setImageDrawable(ContextCompat.getDrawable(context, R.drawable.ic_win));
                    binding.textGameRound.setText(context.getString(R.string.game_over));
                    break;
            }

            binding.cardItemGame.setOnClickListener(v -> callback.onGameItemClicked(game));
        }
    }


    // === INTERFACE

    /**
     * Interface to handle events on a cell
     */
    public interface OnGameItemInteraction {
        void onGameItemClicked(Game game);
    }
}
