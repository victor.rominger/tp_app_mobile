package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data;

import com.google.firebase.firestore.Exclude;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Data class to hold information about a round
 */
public class Round {

    // === ATTRIBUTES

    private String title;
    private List<Step> steps;


    // === CONSTRUCTORS

    public Round() { }

    public Round(String title, List<Step> steps) {
        this.title = title;
        this.steps = steps;
    }


    // === GETTERS AND SETTERS

    public String getTitle() {
        return title;
    }

    public List<Step> getSteps() {
        return steps;
    }

    @Exclude public void setSteps(List<Step> steps) {
        this.steps = steps;
    }


    // === PUBLIC METHODS

    @Exclude public int getStepWinner(int step, int user) {
        double s1 = steps.get(step).distanceBetween(user);
        double s2 = steps.get(step).distanceBetween(user ^ 1);

        if (user == 0) {
            return compare(s1, s2);
        } else {
            return compare(s2, s1);
        }
    }


    // === PRIVATE METHODS

    @Exclude private int compare(double x, double y) {
        if (x == y) {
            return -1;
        } else if (x < y) {
            return 0;
        } else {
            return 1;
        }
    }
}
