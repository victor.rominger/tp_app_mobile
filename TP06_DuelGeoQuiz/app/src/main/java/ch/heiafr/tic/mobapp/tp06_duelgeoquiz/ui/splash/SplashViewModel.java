package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.splash;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.io.InputStream;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.GameBuilder;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.AuthenticationRepository;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.UserRepository;

/**
 * ViewModel class linked to the splash screen which initialise the application.
 */
public class SplashViewModel extends AndroidViewModel {

    private final Application application;
    private final AuthenticationRepository authenticationRepository;
    private final UserRepository userRepository;

    private final MutableLiveData<FirebaseResultState> result = new MutableLiveData<>(null);

    public SplashViewModel(@NonNull Application application) {
        super(application);
        this.application = application;

        authenticationRepository = AuthenticationRepository.getInstance();
        userRepository = UserRepository.getInstance();
    }

    public void authenticateUser() {
        SharedPreferences sharedPreferences = application.getSharedPreferences(application.getString(R.string.pref_file_key), Context.MODE_PRIVATE);
        String email = sharedPreferences.getString(application.getString(R.string.pref_remember_email_key), null);
        String password = sharedPreferences.getString(application.getString(R.string.pref_remember_password_key), null);

        if (email == null || password == null) {
            result.setValue(FirebaseResultState.FAILURE);
        } else {
            authenticationRepository.signIn(email, password, signInResultState -> {
                if (signInResultState == FirebaseResultState.SUCCESS) {
                    userRepository.fetchUser(authenticationRepository.getFirebaseUser().getUid(), getUserResult -> result.postValue(signInResultState));
                } else {
                    result.postValue(signInResultState);
                }
            });
        }
    }

    public void loadData(InputStream streamContinents, InputStream streamCountries, InputStream streamCities) {
        GameBuilder.loadData(streamContinents, streamCountries, streamCities);
        authenticateUser();
    }

    public LiveData<FirebaseResultState> getResult() {
        return result;
    }
}
