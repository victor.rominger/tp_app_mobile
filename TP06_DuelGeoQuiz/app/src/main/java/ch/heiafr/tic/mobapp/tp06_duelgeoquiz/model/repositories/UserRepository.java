package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;
import com.google.firebase.firestore.WriteBatch;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Game;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.User;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.OnRepositoryEvent;

import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.USERS_COLLECTION;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.GAMES_COLLECTION;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.UID_FIELD;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.UIDS_FIELD;

public class UserRepository {

    // === SINGLETON

    private UserRepository() {}

    public static UserRepository getInstance() {
        if (instance == null) {
            instance = new UserRepository();
        }
        return instance;
    }

    // === ATTRIBUTES

    private final FirebaseFirestore firebaseFirestore = FirebaseFirestore.getInstance();
    private static volatile UserRepository instance;
    private final MutableLiveData<List<User>> users = new MutableLiveData<>(null);
    private User loggedUser;


    // === PUBLIC METHODS

    public User getLoggedUser() {
        return loggedUser;
    }

    public LiveData<List<User>> getUsers() {
        return users;
    }

    public void createUser(String uid, String username, String title, OnRepositoryEvent callback) {
        User user = new User(uid, username, title);
        this.firebaseFirestore.collection(USERS_COLLECTION)
                .document(uid)
                .set(user)
                .addOnCompleteListener(
                        new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull @NotNull Task<Void> task) {
                                if (task.isSuccessful()) {
                                    loggedUser = user;
                                    setupRealTimeListeners();
                                    callback.onFirebaseResult(FirebaseResultState.SUCCESS);
                                    return;
                                }
                                callback.onFirebaseResult(FirebaseResultState.FAILURE);
                            }
                        }
                );
    }

    // Fetches the information of a single user
    public void fetchUser(String uid, OnRepositoryEvent callback) {
        this.firebaseFirestore.collection(USERS_COLLECTION)
                .document(uid)
                .get()
                .addOnCompleteListener(
                        new OnCompleteListener<DocumentSnapshot>() {
                            @Override
                            public void onComplete(@NonNull @NotNull Task<DocumentSnapshot> task) {
                                if (task.isSuccessful()) {
                                    DocumentSnapshot documentSnapshot = (DocumentSnapshot) task.getResult();
                                    if (documentSnapshot.exists()) {
                                        loggedUser = (User) documentSnapshot.toObject(User.class);
                                        setupRealTimeListeners();
                                        callback.onFirebaseResult(FirebaseResultState.SUCCESS);
                                        return;
                                    }
                                }
                                callback.onFirebaseResult(FirebaseResultState.FAILURE);
                            }
                        }
                );
    }

    // Fetches all the users in the database
    public void fetchUsers(OnRepositoryEvent callback) {
        User user = getInstance().getLoggedUser();
        this.firebaseFirestore.collection(USERS_COLLECTION)
                .whereNotEqualTo(UID_FIELD, user.getUid())
                .get()
                .addOnCompleteListener(
                        new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull @NotNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    ArrayList<User> arrayList = new ArrayList<>();
                                    for (QueryDocumentSnapshot queryDocumentSnapshot : (QuerySnapshot) Objects.requireNonNull(task.getResult())) {
                                        arrayList.add(queryDocumentSnapshot.toObject(User.class));
                                    }
                                    users.setValue(arrayList);
                                    callback.onFirebaseResult(FirebaseResultState.SUCCESS);
                                    return;
                                }
                                callback.onFirebaseResult(FirebaseResultState.FAILURE);
                            }
                        }
                );
    }

    // Updates the user information (NEEDS TO KEEP DATA IN SYNC - DENORMALISATION !!!)
    public void updateUser(User user, OnRepositoryEvent callback) {
        this.loggedUser = user;
        WriteBatch writeBatch = this.firebaseFirestore.batch();
        writeBatch.set(
                this.firebaseFirestore
                        .collection(USERS_COLLECTION)
                        .document(user.getUid()),
                this.loggedUser,
                SetOptions.merge());
        this.firebaseFirestore.collection(GAMES_COLLECTION)
                .whereArrayContains(UIDS_FIELD, user.getUid())
                .get()
                .addOnCompleteListener(
                        new OnCompleteListener<QuerySnapshot>() {
                            @Override
                            public void onComplete(@NonNull @NotNull Task<QuerySnapshot> task) {
                                if (task.isSuccessful()) {
                                    for (DocumentSnapshot documentSnapshot : (QuerySnapshot) Objects.requireNonNull(task.getResult())) {
                                        Game g = (Game) documentSnapshot.toObject(Game.class);
                                        assert g != null;
                                        g.setPosition(g.getUids().indexOf(user.getUid()));
                                        g.updateUser(user);
                                        writeBatch.set(documentSnapshot.getReference(), g, SetOptions.merge());
                                    }
                                    writeBatch.commit().addOnCompleteListener(
                                            new OnCompleteListener() {
                                                @Override
                                                public final void onComplete(Task task) {
                                                    if (task.isSuccessful()) {
                                                        callback.onFirebaseResult(FirebaseResultState.SUCCESS);
                                                    } else {
                                                        callback.onFirebaseResult(FirebaseResultState.FAILURE);
                                                    }
                                                }
                                            });
                                    return;
                                }
                                callback.onFirebaseResult(FirebaseResultState.FAILURE);
                            }
                        }
                );
    }


    // === PRIVATE METHODS

    // Enable listeners to allow the real time feature
    private void setupRealTimeListeners() {
        this.firebaseFirestore.collection(USERS_COLLECTION)
                .whereNotEqualTo(UID_FIELD, this.loggedUser.getUid())
                .addSnapshotListener(
                        new EventListener<QuerySnapshot>() {
                            @Override
                            public void onEvent(@Nullable @org.jetbrains.annotations.Nullable QuerySnapshot value, @Nullable @org.jetbrains.annotations.Nullable FirebaseFirestoreException error) {
                                if (error == null) {
                                    ArrayList<User> arrayList = new ArrayList<User>();
                                    assert value != null;
                                    for (QueryDocumentSnapshot queryDocumentSnapshot : value) {
                                        arrayList.add(queryDocumentSnapshot.toObject(User.class));
                                    }
                                    users.setValue(arrayList);
                                }
                            }
                        }
                );
    }
}
