package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.home;

import android.app.Application;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.Transformations;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.MainActivity;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Game;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories.GameRepository;

/**
 * ViewModel class linked to the home screen including the list of games.
 */
public class HomeViewModel extends AndroidViewModel {

    // === ATTRIBUTES

    private final Application application;
    private final GameRepository gameRepository;

    private final MutableLiveData<FirebaseResultState> result = new MutableLiveData<>(null);
    private final MutableLiveData<List<Game>> games = new MutableLiveData<>(null);


    // === CONSTRUCTORS

    public HomeViewModel(@NonNull Application application) {
        super(application);
        this.application = application;

        gameRepository = GameRepository.getInstance();

        fetchGames();
        handleNotif();

    }


    // === GETTERS AND SETTERS

    public LiveData<List<Game>> getGames() {
        return games;
    }

    public void setGame(Game game) {
        gameRepository.setCurrentGame(game);
    }

    public LiveData<FirebaseResultState> getResult() {
        return result;
    }

    public LiveData<List<Game>> getGameList() {
        return Transformations.map(gameRepository.getGames(), input -> {
            games.setValue(input);
            if (input != null) {
                return input;
            } else {
                return new ArrayList<>();
            }
        });
    }

    public void handleNotif() {
        Random r = new Random();
        this.gameRepository.getNotification().observeForever(
                new Observer<FirebaseResultState>() {
                    @Override
                    public void onChanged(FirebaseResultState firebaseResultState) {
                        if (firebaseResultState == FirebaseResultState.SUCCESS) {
                            Intent intent =
                                    new Intent(
                                            getApplication().getBaseContext(),
                                            MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                            NotificationManagerCompat
                                    .from(getApplication().getBaseContext())
                                    .notify(
                                            0, // same id to prevent several notifications in notifications center
                                            new NotificationCompat.Builder(getApplication().getBaseContext(),
                                                    application.getString(R.string.channel_id))
                                                    .setSmallIcon(R.drawable.ic_logo_outline)
                                                    .setContentTitle(application.getString(R.string.notification_title))
                                                    .setContentText(application.getString(R.string.notification_text))
                                                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                                                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                                                    .setContentIntent(PendingIntent.getActivity(getApplication().getBaseContext(), 0, intent, 0))
                                                    .setAutoCancel(true)
                                                    .build()
                                    );
                        }
                    }
                }
        );
    }

    // === PUBLIC METHODS

    public void fetchGames() {
        result.setValue(FirebaseResultState.LOADING);
        gameRepository.fetchGames(result::postValue);
    }
}
