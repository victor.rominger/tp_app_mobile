package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.repositories;


import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.OnRepositoryEvent;
import com.google.android.gms.tasks.Task;
import org.jetbrains.annotations.NotNull;

/**
 * Repository class that manages the authentication feature of the application.
 */

public class AuthenticationRepository {

    // === SINGLETON

    private AuthenticationRepository() {}

    public static AuthenticationRepository getInstance() {
        if (instance == null) {
            instance = new AuthenticationRepository();
        }
        return instance;
    }


    // === ATTRIBUTES

    private static volatile AuthenticationRepository instance;
    private final FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();
    private FirebaseUser firebaseUser = null;

    // === GETTERS AND SETTERS

    public FirebaseUser getFirebaseUser() {
        return this.firebaseUser;
    }


    // === PUBLIC METHODS

    // Signs the user out
    public void signOut() {
        this.firebaseAuth.signOut();
        this.firebaseUser = null;
    }

    // Signs the user in
    public void signIn(String email, String password, OnRepositoryEvent callback) {
        this.firebaseAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            firebaseUser = firebaseAuth.getCurrentUser();
                            callback.onFirebaseResult(FirebaseResultState.SUCCESS);
                            return;
                        }
                        firebaseUser = null;
                        callback.onFirebaseResult(FirebaseResultState.FAILURE);
                    }
                }
        );
    }

    // Signs the user up
    public void signUp(String email, String password, OnRepositoryEvent callback) {
        this.firebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(
                new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            firebaseUser = firebaseAuth.getCurrentUser();
                            callback.onFirebaseResult(FirebaseResultState.SUCCESS);
                            return;
                        }
                        firebaseUser = null;
                        callback.onFirebaseResult(FirebaseResultState.FAILURE);
                    }
                }
        );
    }

    // Updates the user's email address
    public void updateEmail(String email, OnRepositoryEvent callback) {
        this.firebaseUser.updateEmail(email).addOnCompleteListener(
                new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull @NotNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            callback.onFirebaseResult(FirebaseResultState.SUCCESS);
                        } else {
                            callback.onFirebaseResult(FirebaseResultState.FAILURE);
                        }
                    }
                }
        );
    }
}
