package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.sign_up;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.BaseFragment;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.databinding.FragmentSignUpBinding;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;

/**
 * Fragment class managing the sign up screen.
 */
public class SignUpFragment extends BaseFragment {

    // === ATTRIBUTES

    private FragmentSignUpBinding binding;


    // === FRAGMENT OVERRIDES

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sign_up, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbarVisibility = View.GONE;

        SignUpViewModel viewModel = new ViewModelProvider(this).get(SignUpViewModel.class);
        NavController navController = Navigation.findNavController(view);

        binding.buttonConnectAccount.setOnClickListener(v -> navController.navigateUp());

        binding.buttonSignUp.setOnClickListener(v -> {
            String username = binding.editSignUpUsername.getText().toString();
            String email = binding.editSignUpEmail.getText().toString();
            String password = binding.editSignUpPassword.getText().toString();
            viewModel.signUp(username, email, password);
        });

        viewModel.getResult().observe(getViewLifecycleOwner(), firebaseResult -> {
            if (firebaseResult == null) return;

            if (firebaseResult == FirebaseResultState.SUCCESS) {
                navController.navigate(R.id.action_destination_sign_up_to_destination_home);
            } else if (firebaseResult == FirebaseResultState.FAILURE) {
                Toast.makeText(getContext(), getString(R.string.toast_error_sign_up), Toast.LENGTH_SHORT).show();
            }
        });

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
    }
}