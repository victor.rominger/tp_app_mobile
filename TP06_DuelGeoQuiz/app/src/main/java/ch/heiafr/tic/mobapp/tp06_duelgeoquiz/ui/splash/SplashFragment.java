package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.splash;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.InputStream;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.BaseFragment;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;

import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.CITIES_FILE;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.CONTINENTS_FILE;
import static ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.utils.Constants.COUNTRIES_FILE;

/**
 * Fragment class managing the splash screen which initialise the application.
 */
public class SplashFragment extends BaseFragment {

    // === FRAGMENT OVERRIDES

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_splash, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbarVisibility = View.GONE;

        SplashViewModel viewModel = new ViewModelProvider(this).get(SplashViewModel.class);

        viewModel.getResult().observe(getViewLifecycleOwner(), firebaseResult -> {
            if (firebaseResult == null) {
                return;
            }

            NavController navController = NavHostFragment.findNavController(this);
            if (firebaseResult == FirebaseResultState.SUCCESS) {
                navController.navigate(R.id.action_destination_splash_to_destination_home);
            } else if (firebaseResult == FirebaseResultState.FAILURE) {
                navController.navigate(R.id.action_destination_splash_to_destination_sign_in);
            }
        });

        try {
            InputStream streamContinents = getContext().getAssets().open(CONTINENTS_FILE);
            InputStream streamCountries = getContext().getAssets().open(COUNTRIES_FILE);
            InputStream streamCities = getContext().getAssets().open(CITIES_FILE);
            viewModel.loadData(streamContinents, streamCountries, streamCities);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}