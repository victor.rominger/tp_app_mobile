package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.home;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.BaseFragment;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.databinding.FragmentHomeBinding;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.Game;

/**
 * Fragment class managing the home screen including the list of games.
 */
public class HomeFragment extends BaseFragment {

    // === ATTRIBUTES

    private FragmentHomeBinding binding;
    private HomeViewModel viewModel;


    // === FRAGMENT OVERRIDES

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbarVisibility = View.VISIBLE;

        viewModel = new ViewModelProvider(this).get(HomeViewModel.class);

        HomeAdapter adapter = new HomeAdapter(new ArrayList<>(), game -> {
            viewModel.setGame(game);
            NavController navController = NavHostFragment.findNavController(this);
            navController.navigate(R.id.action_destination_home_to_destination_game_summary);
        });

        viewModel.getGameList().observe(getViewLifecycleOwner(), games -> {
            updateUI(viewModel.getResult().getValue(), games);
            adapter.updateList(games);
        });

        viewModel.getResult().observe(getViewLifecycleOwner(), firebaseResult -> updateUI(firebaseResult, viewModel.getGames().getValue()));

        binding.listHome.setAdapter(adapter);
        binding.listHome.setLayoutManager(new LinearLayoutManager(getContext()));
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_home, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        NavController navController = NavHostFragment.findNavController(this);
        if (item.getItemId() == R.id.menu_new_game) {
            navController.navigate(R.id.action_destination_home_to_destination_play);
            return true;
        } else if (item.getItemId() == R.id.menu_settings) {
            navController.navigate(R.id.action_destination_home_to_destination_settings);
            return true;
        } else if (item.getItemId() == R.id.menu_refresh) {
            viewModel.fetchGames();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    // === PRIVATE METHODS

    private void updateUI(FirebaseResultState firebaseResultState, List<Game> games) {
        if (firebaseResultState == null || games == null) {
            return;
        }

        if (firebaseResultState == FirebaseResultState.SUCCESS) {
            if (games.size() > 0) {
                binding.listHome.setVisibility(View.VISIBLE);
                binding.groupNoGame.setVisibility(View.INVISIBLE);
            } else {
                binding.listHome.setVisibility(View.INVISIBLE);
                binding.groupNoGame.setVisibility(View.VISIBLE);
            }
            binding.progressHome.setVisibility(View.INVISIBLE);
        }
    }
}