package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavController;
import androidx.navigation.fragment.NavHostFragment;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.BaseFragment;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.databinding.FragmentSettingsBinding;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.enums.FirebaseResultState;

/**
 * Fragment class managing the settings screen.
 */
public class SettingsFragment extends BaseFragment {

    // === ATTRIBUTES

    private FragmentSettingsBinding binding;
    private SettingsViewModel viewModel;
    private NavController navController;


    // === CONSTRUCTORS

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_settings, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        toolbarVisibility = View.VISIBLE;

        viewModel = new ViewModelProvider(this).get(SettingsViewModel.class);
        navController = NavHostFragment.findNavController(this);

        binding.layoutSettingsUsername.getEditText().setText(viewModel.getUser().getUsername());
        binding.layoutSettingsTitle.getEditText().setText(viewModel.getUser().getTitle());
        binding.layoutSettingsEmail.getEditText().setText(viewModel.getFirebaseUser().getEmail());

        viewModel.getResult().observe(getViewLifecycleOwner(), firebaseResult -> {
            if (firebaseResult == null) return;

            if (firebaseResult == FirebaseResultState.SUCCESS) {
                navController.navigateUp();
                Toast.makeText(getContext(), getString(R.string.toast_success_settings), Toast.LENGTH_SHORT).show();
            } else if (firebaseResult == FirebaseResultState.FAILURE) {
                setToolbarVisibility(View.VISIBLE);
                Toast.makeText(getContext(), getString(R.string.toast_error_settings), Toast.LENGTH_SHORT).show();
            }
        });

        binding.setViewModel(viewModel);
        binding.setLifecycleOwner(this);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_settings, menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == R.id.menu_sign_out) {
            viewModel.signOut();
            navController.navigate(R.id.action_destination_settings_to_destination_sign_in);
            return true;
        } else if (item.getItemId() == R.id.menu_save) {
            setToolbarVisibility(View.GONE);
            viewModel.saveSettings(
                    binding.editSettingsUsername.getText().toString(),
                    binding.editSettingsTitle.getText().toString(),
                    binding.editSettingsEmail.getText().toString());
        }

        return super.onOptionsItemSelected(item);
    }
}