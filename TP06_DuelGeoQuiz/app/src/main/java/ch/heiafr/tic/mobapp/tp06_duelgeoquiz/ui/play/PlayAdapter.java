package ch.heiafr.tic.mobapp.tp06_duelgeoquiz.ui.play;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.R;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.databinding.ItemUserBinding;
import ch.heiafr.tic.mobapp.tp06_duelgeoquiz.model.data.User;

/**
 * Recycler adapter class managing the list of users.
 */
public class PlayAdapter extends RecyclerView.Adapter<PlayAdapter.UserViewHolder> {

    // === ATTRIBUTES

    private List<User> items;
    private final PlayViewModel viewModel;
    private final OnUserItemInteraction callback;


    // === CONSTRUCTORS

    public PlayAdapter(List<User> items, PlayViewModel viewModel, OnUserItemInteraction callback) {
        this.items = items;
        this.viewModel = viewModel;
        this.callback = callback;
    }


    // === ADAPTER OVERRIDES

    @NonNull
    @Override
    public PlayAdapter.UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemUserBinding userBinding = ItemUserBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new UserViewHolder(userBinding);
    }

    @Override
    public void onBindViewHolder(@NonNull PlayAdapter.UserViewHolder holder, int position) {
        holder.bind(items.get(position), callback);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }


    // === PUBLIC METHODS

    public void updateList(List<User> items) {
        this.items = items;
        notifyDataSetChanged();
    }


    // === VIEW HOLDERS

    /**
     * View holder class managing a user cell.
     */
    public static class UserViewHolder extends RecyclerView.ViewHolder {

        final ItemUserBinding binding;

        public UserViewHolder(ItemUserBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }

        public void bind(User user, OnUserItemInteraction callback) {
            Context context = itemView.getContext();
            int avatarID = context.getResources().getIdentifier(user.getAvatar(), "drawable", context.getPackageName());

            binding.imageItemUserAvatar.setImageResource(avatarID);
            binding.textItemUserUsername.setText(user.getUsername());
            binding.textItemUserLevel.setText(user.getTitle());

            binding.cardItemUser.setOnClickListener(v -> callback.onUserItemClicked(user));
        }
    }


    // === INTERFACE

    /**
     * Interface to handle events on a cell
     */
    public interface OnUserItemInteraction {
        void onUserItemClicked(User user);
    }
}
