package ch.heiafr.tic.tp02_moleswhacker;

import android.widget.ImageView;

import java.util.ArrayList;

public class GameStateContainer {
    private long time;
    private int score;
    private boolean isLaunched;
    public long COUNTDOWN_TICK = 1000; // tick unit is ms so this is 1 second
    public int MOLES_NUMBER = 12; // amount of moles in the game
    private final ArrayList<ImageView> views = new ArrayList<ImageView>(MOLES_NUMBER);


    public GameStateContainer() {
        this.time = 0;
        this.score = 0;
        this.isLaunched = false;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getScore() {
        return score;
    }

    public void incrementScore() {
        this.score++;
    }

    public void resetScore() {
        this.score = 0;
    }

    public boolean isLaunched() {
        return isLaunched;
    }

    public void setLaunched(boolean launched) {
        isLaunched = launched;
    }

    public ArrayList<ImageView> getViews() {
        return views;
    }

    public void setView(ImageView view)
    {
        views.add(view);
    }

    public void setScore(int score) {
        this.score = score;
    }
}
