package ch.heiafr.tic.tp02_moleswhacker;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private final long GAME_DURATION = 30000; // game duration in ms --> 30s
    private final Random random = new Random();
    private GameStateContainer gameState = new GameStateContainer();
    private GameTimer timer;
    private Button startButton;
    private TextView txtTimeTitle, txtTimeValue, txtScoreTitle, txtScoreValue;
    private int currentMole = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("Info", "Creating Interface");
        setContentView(R.layout.activity_main);

        startButton = findViewById(R.id.buttonStart);
        txtTimeTitle = findViewById(R.id.txtTimeTitle);
        txtScoreTitle = findViewById(R.id.txtScoreTitle);
        txtTimeValue = findViewById(R.id.txtTimeValue);
        txtScoreValue = findViewById(R.id.txtScoreValue);

        for (int i = 0; i < gameState.MOLES_NUMBER; i++) {
            String moleID = "mole" + (i + 1) + "";
            Log.i("info", moleID);
            int resID = getResources().getIdentifier(moleID, "id", getPackageName());
            gameState.setView(findViewById(resID));
        }

        timer = GameTimer.getInstance(new GameTimer.OnGameTimerEvent() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onTick(long l) {
                gameState.setTime(l / gameState.COUNTDOWN_TICK);
                txtTimeValue.setText(Long.toString(gameState.getTime()));
            }

            @Override
            public void onFinish() {
                Log.i("Info", "Game Over !");
                gameState.setLaunched(false);
                for (int i = 0; i < gameState.getViews().size(); i++) {
                    gameState.getViews().get(i).setVisibility(View.VISIBLE);
                }
                startButton.setEnabled(true);
            }
        });

        handleStartGame();
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i("Info", "Savingdata post screen orientation");
        for (int i = 0; i < gameState.getViews().size(); i++) {
            outState.putInt(Integer.toString(i), gameState.getViews().get(i).getVisibility());
        }
        outState.putBoolean("startButtonState", startButton.isEnabled());
        outState.putBoolean("gameState", gameState.isLaunched());
        outState.putString("score", txtScoreValue.getText().toString());
        outState.putString("time", txtTimeValue.getText().toString());
    }

    @Override
    public void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i("Info", "Restoring data post screen orientation");
        startButton.setEnabled(savedInstanceState.getBoolean("startButtonState"));
        txtScoreValue.setText(savedInstanceState.getString("score"));
        gameState.setScore(Integer.parseInt(savedInstanceState.getString("score")));
        txtTimeValue.setText(savedInstanceState.getString("time"));
        gameState.setLaunched(savedInstanceState.getBoolean("gameState"));
        for (int i = 0; i < gameState.getViews().size(); i++) {
            gameState.getViews().get(i).setOnClickListener(new View.OnClickListener() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onClick(View view) {
                    if (!gameState.isLaunched()) return;
                    gameState.incrementScore();
                    txtScoreValue.setText(Integer.toString(gameState.getScore()));
                    view.setVisibility(View.INVISIBLE);
                    displayRandomMole();
                }
            });
            gameState.getViews().get(i)
                    .setVisibility(savedInstanceState.getInt(Integer.toString(i)));
        }
    }


    public void onPause() {
        super.onPause();
        Log.i("Info", "Actually on Pause");
        timer.pauseTimer();
    }

    public void onResume() {
        super.onResume();
        Log.i("Info", "Actually resuming game");
        timer.resumeTimer();
    }

    public void onStart() {
        super.onStart();
        Log.i("Info", "Actually starting game");
    }

    public void onStop() {
        super.onStop();
        Log.i("Info", "Actually stopping game");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i("Info", "Actually destroying");
    }

    private void handleStartGame() {
        Log.i("Info", "Starting Game !");

        startButton.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View view) {
                if (gameState.isLaunched()) {
                    Log.e("Error", "The game is already running, " +
                            "if you see that there is a glitch");
                    return;
                }
                gameState.resetScore();
                txtScoreValue.setText(Integer.toString(gameState.getScore()));
                startButton.setEnabled(false);
                gameState.setLaunched(true);
                for (int i = 0; i < gameState.getViews().size(); i++) {
                    gameState.getViews().get(i).setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (!gameState.isLaunched()) return;
                            gameState.incrementScore();
                            txtScoreValue.setText(Integer.toString(gameState.getScore()));
                            view.setVisibility(View.INVISIBLE);
                            displayRandomMole();
                        }
                    });
                    gameState.getViews().get(i).setVisibility(View.INVISIBLE);
                }
                displayRandomMole();
                timer.startTimer(GAME_DURATION, gameState.COUNTDOWN_TICK);
            }
        });
    }

    private void displayRandomMole() {
        int moleID = currentMole;
        while (moleID == currentMole) {
            moleID = random.nextInt(gameState.MOLES_NUMBER);
        }
        currentMole = moleID;
        gameState.getViews().get(moleID).setVisibility(View.VISIBLE);
    }
}