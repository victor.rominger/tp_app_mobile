package com.example.tp01_diceroler;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.Random;

public class MainActivity extends AppCompatActivity {

    private final int DICE_NUMBER = 6;              // New
    private final Random random = new Random();     // New
    private final ImageView[] views = new ImageView[2];

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Log.i("Update", "Creating new instance");

        Button roll = findViewById(R.id.button);

        ImageView vDice1;
        ImageView vDice2;

        vDice1 = findViewById(R.id.imageViewDice1);
        vDice2 = findViewById(R.id.imageViewDice2);
        vDice2.setVisibility(View.GONE);        // Don't display Dice 2 at beginning

        views[0] = vDice1;
        views[1] = vDice2;

        Button mode1 = findViewById(R.id.select1);
        Button mode2 = findViewById(R.id.select2);

        StoreState state = new StoreState();

        displayWithDiceRollState(roll, views, state);

        changeMode(mode1, state, vDice2);
        changeMode(mode2, state, vDice2);
    }

    // To be cleaner we could pass ImageView array with dices
    private void displayWithDiceRollState(Button roll, ImageView[] vDices, StoreState state) {
        Log.i("Update", "State: " + state.getModeState());

        roll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i = 0; i < state.getModeState(); i++)
                {
                    displayRandom(vDices[i]);
                }
            }
        });
    }

    private static class StoreState {
        private int modeState = 1;

        public void setModState(int state) {
            this.modeState = state;
        }

        public int getModeState() {
            return modeState;
        }
    }

    private void changeMode(Button mode, StoreState storage, ImageView dice) {

        mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.i("Update", "Updating the amount of dices");
                changeParameters(mode, storage, dice);
            }
        });
    }

    private void changeParameters(Button mode, StoreState state, ImageView dice) {
        CharSequence text = mode.getText();
        if ("1".contentEquals(text)) {
            dice.setVisibility(View.GONE);
            state.setModState(1);
        } else if ("2".contentEquals(text)) {
            dice.setVisibility(View.VISIBLE);
            state.setModState(2);
        }
    }

    private void displayRandom(ImageView dice) {

        int value = random.nextInt(DICE_NUMBER) + 1;
        switch (value) {
            case 1:
                Log.i("Value", "1");
                dice.setImageResource(R.drawable.ic_dice_1);
                break;
            case 2:
                Log.i("Value", "2");
                dice.setImageResource(R.drawable.ic_dice_2);
                break;
            case 3:
                Log.i("Value", "3");
                dice.setImageResource(R.drawable.ic_dice_3);
                break;
            case 4:
                Log.i("Value", "4");
                dice.setImageResource(R.drawable.ic_dice_4);
                break;
            case 5:
                Log.i("Value", "5");
                dice.setImageResource(R.drawable.ic_dice_5);
                break;
            case 6:
                Log.i("Value", "6");
                dice.setImageResource(R.drawable.ic_dice_6);
                break;
            default:
                dice.setImageResource(R.drawable.ic_dice_empty);
                break;
        }
    }
}