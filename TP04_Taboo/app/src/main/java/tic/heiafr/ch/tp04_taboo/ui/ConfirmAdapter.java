package tic.heiafr.ch.tp04_taboo.ui;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import tic.heiafr.ch.tp04_taboo.R;
import tic.heiafr.ch.tp04_taboo.taboo.Taboo;

public class ConfirmAdapter extends RecyclerView.Adapter<ConfirmAdapter.ViewHolder> {

    private List<Taboo> list;
    private ConfirmAdapter.OnItemClickListener listener;

    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView cardName;
        public ImageView image;

        public ViewHolder(View view) {
            super(view);
            cardName = view.findViewById(R.id.card_name);
            image = view.findViewById(R.id.state_image);
        }

        public void setOnClickListener(
                ConfirmAdapter.OnItemClickListener listener,
                int position
        ) {
            itemView.setOnClickListener(v -> listener.onItemClick(position));
        }

    }

    public ConfirmAdapter(List<Taboo> list, ConfirmAdapter.OnItemClickListener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_cell, parent, false);
        return new ViewHolder(v);
    }

    @Override // Call each time when item in [position] is created or updated
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.cardName.setText(list.get(position).getWord());
        switch (list.get(position).getState()) {
            case PASSED:
                holder.image.setImageResource(R.drawable.ic_passed);
                break;
            case WON:
                holder.image.setImageResource(R.drawable.ic_correct);
                break;
            case FAILED:
                holder.image.setImageResource(R.drawable.ic_wrong);
                break;
        }
        holder.setOnClickListener(listener, position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
