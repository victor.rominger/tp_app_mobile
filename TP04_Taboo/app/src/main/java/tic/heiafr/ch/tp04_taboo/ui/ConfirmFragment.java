package tic.heiafr.ch.tp04_taboo.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import tic.heiafr.ch.tp04_taboo.R;
import tic.heiafr.ch.tp04_taboo.taboo.CardState;
import tic.heiafr.ch.tp04_taboo.taboo.Taboo;
import tic.heiafr.ch.tp04_taboo.taboo.TabooManager;

public class ConfirmFragment extends Fragment {
    TabooManager tabooManager;
    RecyclerView recyclerView;
    private View root;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_confirm, container, false);
        tabooManager = TabooManager.getInstance(null);
        recyclerView = this.root.findViewById(R.id.confirm_recycler_view);
        List<Taboo> activatedCards = new ArrayList<>();
        tabooManager.getPlayedThisRound().forEach((card) -> {
            if ((card.getState() != null)) {
                activatedCards.add(card);
            }
        });
        if(activatedCards.size() != 0) {
            ConfirmAdapter confirmAdapter = new ConfirmAdapter(activatedCards,
                    position -> {
                        switch (tabooManager.getPlayedThisRound().get(position).getState()) {
                            case WON:
                                tabooManager.getPlayedThisRound().get(position)
                                        .setState(CardState.PASSED);
                                break;
                            case PASSED:
                                tabooManager.getPlayedThisRound().get(position)
                                        .setState(CardState.FAILED);
                                break;
                            case FAILED:
                                tabooManager.getPlayedThisRound().get(position)
                                        .setState(CardState.WON);
                                break;
                            default:
                                break;
                        }
                        recyclerView.getAdapter().notifyDataSetChanged();
                    }
            );
            recyclerView.setAdapter(confirmAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this.getActivity()));
        }

        return this.root;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.confirm_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.validate_btn) {
            tabooManager.validateTurn();

            FragmentManager fm = getActivity().getSupportFragmentManager();
            if (tabooManager.isGameOver()) {
                Navigation.findNavController(this.root).navigate(R.id.action_confirmFragment_to_endFragment);
            } else {
                ConfirmFragmentDirections.ActionConfirmFragmentToPlayFragment action =
                        ConfirmFragmentDirections.actionConfirmFragmentToPlayFragment(false, null,
                                0, 0, 0, 0);
                action.setIsNewGame(false);
                Navigation.findNavController(this.root).navigate(action);

            }
            return true;
        }
        return super.onContextItemSelected(item);
    }
}
