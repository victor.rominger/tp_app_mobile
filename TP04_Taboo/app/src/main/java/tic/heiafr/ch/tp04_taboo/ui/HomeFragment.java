package tic.heiafr.ch.tp04_taboo.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.widget.SwitchCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.Navigation;

import java.util.ArrayList;

import tic.heiafr.ch.tp04_taboo.R;

public class HomeFragment extends Fragment {

    /*public static final String CODE_DIFFICULT = "code_difficult";
    public static final String CODE_TEAMS = "code_teams";
    public static final String CODE_PASS = "code_pass";
    public static final String CODE_DURATION = "code_duration";
    public static final String CODE_CARDS = "code_cards";
    public static final String CODE_ERROR_PENALTY = "code_error_penalty";
    public static final String CODE_NEW_GAME = "code_new_game";*/

    private EditText team1;
    private EditText team2;
    private EditText team3;
    private EditText team4;
    private EditText numberOfCards;
    private EditText turnDuration;
    private EditText numberOfPass;
    private EditText errorPenalty;
    private SwitchCompat difficultSwitch;
    private View root;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Initialise the activity
     *
     * @param savedInstanceState previous saved state data
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_home, container, false);

        initUI();

        return this.root;
    }

    /**
     * Fetch the views and validate the input fields.
     */
    private void initUI() {
        team1 = this.root.findViewById(R.id.team_1);
        team2 = this.root.findViewById(R.id.team_2);
        team3 = this.root.findViewById(R.id.team_3);
        team4 = this.root.findViewById(R.id.team_4);
        numberOfCards = this.root.findViewById(R.id.number_of_cards);
        turnDuration = this.root.findViewById(R.id.turn_duration);
        numberOfPass = this.root.findViewById(R.id.number_of_pass);
        difficultSwitch = this.root.findViewById(R.id.difficult_switch);
        errorPenalty = this.root.findViewById(R.id.error_penalty);
        Button playButton = this.root.findViewById(R.id.play_button);

        errorPenalty.setOnEditorActionListener(editorListener);

        playButton.setOnClickListener((view) -> this.playGame());
    }

    /**
     * Launch the activity where the game will happen
     *
     * @param isHard        taboos difficulty
     * @param teams         team names
     * @param numberOfCards number of taboos for the whole game
     * @param turnDuration  available time per turn
     * @param numberOfPass  available passes per turn
     * @param errorPenalty  penalty when a taboo is wrongly guessed
     */
    private void playGame(boolean isHard, ArrayList<String> teams, int numberOfCards, int turnDuration, int numberOfPass, int errorPenalty) {

        /*this.getActivity().getIntent().putExtra(CODE_DIFFICULT, isHard);
        this.getActivity().getIntent().putExtra(CODE_TEAMS, teams);
        this.getActivity().getIntent().putExtra(CODE_CARDS, numberOfCards);
        this.getActivity().getIntent().putExtra(CODE_DURATION, turnDuration);
        this.getActivity().getIntent().putExtra(CODE_PASS, numberOfPass);
        this.getActivity().getIntent().putExtra(CODE_ERROR_PENALTY, errorPenalty);
        this.getActivity().getIntent().putExtra(CODE_NEW_GAME, true);*/

        HomeFragmentDirections.ActionHomeFragmentToPlayFragment action =
                HomeFragmentDirections.actionHomeFragmentToPlayFragment(isHard, teams,
                        numberOfCards, turnDuration, numberOfPass, errorPenalty);
        action.setIsNewGame(true);
        Navigation.findNavController(this.root).navigate(action);

    }

    private final TextView.OnEditorActionListener editorListener = (v, actionId, event) -> {
        switch (actionId) {
            case EditorInfo.IME_ACTION_NEXT:
                break;
            case EditorInfo.IME_ACTION_SEND:
                playGame();
                break;
        }
        return true;
    };

    private void playGame()
    {
        String team1Text = team1.getText().toString().trim();
        String team2Text = team2.getText().toString().trim();
        String team3Text = team3.getText().toString().trim();
        String team4Text = team4.getText().toString().trim();
        String cardsText = numberOfCards.getText().toString().trim();
        String durationText = turnDuration.getText().toString().trim();
        String passText = numberOfPass.getText().toString().trim();
        String errorPenaltyText = errorPenalty.getText().toString().trim();

        ArrayList<String> teamsText = new ArrayList<>();
        if (!team1Text.isEmpty())
            teamsText.add(team1Text);
        if (!team2Text.isEmpty())
            teamsText.add(team2Text);
        if (!team3Text.isEmpty())
            teamsText.add(team3Text);
        if (!team4Text.isEmpty())
            teamsText.add(team4Text);

        if (teamsText.size() < 2 || cardsText.isEmpty() || durationText.isEmpty() || passText.isEmpty() || errorPenaltyText.isEmpty())
            return;

        playGame(difficultSwitch.isChecked(), teamsText,
                Integer.parseInt(cardsText),
                Integer.parseInt(durationText),
                Integer.parseInt(passText),
                Integer.parseInt(errorPenaltyText));
    }
}
