package tic.heiafr.ch.tp04_taboo.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.Navigation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import tic.heiafr.ch.tp04_taboo.R;
import tic.heiafr.ch.tp04_taboo.taboo.Taboo;
import tic.heiafr.ch.tp04_taboo.taboo.TabooManager;

public class PlayFragment extends Fragment implements TabooManager.OnTabooUpdate {
    private TextView timerText;
    private TextView remainingCardsText;
    private TextView currentTeamText;
    private TextView failuresText;
    private TextView availablePass;
    private TextView successText;
    private TextView wordText;
    private TextView taboo1Text;
    private TextView taboo2Text;
    private TextView taboo3Text;
    private TextView taboo4Text;
    private ProgressBar timerBar;
    private View root;

    private TabooManager tabooManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_play, container, false);

        tabooManager = TabooManager.getInstance(this);

        boolean newGame = PlayFragmentArgs.fromBundle(getArguments()).getIsNewGame();

        if (newGame) {
            ArrayList<String> teams = (ArrayList<String>)PlayFragmentArgs.fromBundle(getArguments()).getTeams();
            boolean isHard = PlayFragmentArgs.fromBundle(getArguments()).getIsHard();
            int numberOfCards = PlayFragmentArgs.fromBundle(getArguments()).getNumberOfCards();
            int turnDuration = PlayFragmentArgs.fromBundle(getArguments()).getTurnDuration();
            int numberOfPass = PlayFragmentArgs.fromBundle(getArguments()).getNumberOfPass();
            int errorPenalty = PlayFragmentArgs.fromBundle(getArguments()).getErrorPenalty();

            if (savedInstanceState == null) {
                tabooManager.setupGame(getActivity().getAssets(), isHard, teams, numberOfCards, turnDuration, numberOfPass, errorPenalty);
                tabooManager.startTurn();
            }
        } else {
            if (savedInstanceState == null) {
                tabooManager.startTurn();
            }
        }

        initUI();
        return this.root;
    }

    @Override
    public void onResume() {
        super.onResume();
        tabooManager.resumeTimer();
    }

    @Override
    public void onPause() {
        super.onPause();
        tabooManager.pauseTimer();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        tabooManager.stopTimer();
    }

    private void initUI() {
        timerText = this.root.findViewById(R.id.timer_text);
        remainingCardsText = this.root.findViewById(R.id.remaining_cards);
        currentTeamText = this.root.findViewById(R.id.current_team);
        successText = this.root.findViewById(R.id.success);
        failuresText = this.root.findViewById(R.id.failures);
        availablePass = this.root.findViewById(R.id.available_pass);
        wordText = this.root.findViewById(R.id.word);
        taboo1Text = this.root.findViewById(R.id.taboo_1);
        taboo2Text = this.root.findViewById(R.id.taboo_2);
        taboo3Text = this.root.findViewById(R.id.taboo_3);
        taboo4Text = this.root.findViewById(R.id.taboo_4);
        timerBar = this.root.findViewById(R.id.timer_bar);
        Button errorButton = this.root.findViewById(R.id.error_button);
        Button passButton = this.root.findViewById(R.id.pass_button);
        Button correctButton = this.root.findViewById(R.id.correct_button);

        updateUI();
        timerText.setText(getString(R.string.integer, tabooManager.getTurnDuration()));
        timerBar.setProgress(100);

        errorButton.setOnClickListener((v) -> {
            tabooManager.failure();
            updateUI();
        });

        passButton.setOnClickListener((v) -> {
            tabooManager.pass();
            updateUI();
        });

        correctButton.setOnClickListener((v) -> {
            tabooManager.success();
            updateUI();
        });
    }

    private void updateUI() {
        currentTeamText.setText(getString(R.string.playing_team, tabooManager.getCurrentTeam()));
        remainingCardsText.setText(getString(R.string.remaining_cards, tabooManager.getNumberOfRemainingCards()));
        successText.setText(getString(R.string.integer, tabooManager.getCurrentSuccess()));
        failuresText.setText(getString(R.string.integer, tabooManager.getCurrentFailures()));
        availablePass.setText(getString(R.string.out_of, tabooManager.getAvailablePass(), tabooManager.getNumberOfPass()));

        Taboo card = tabooManager.getCard();
        String[] taboos = card.getTaboos();
        wordText.setText(card.getWord());
        taboo1Text.setText(taboos[0]);
        taboo2Text.setText(taboos[1]);
        taboo3Text.setText(taboos[2]);
        taboo4Text.setText(taboos[3]);
    }

    @Override
    public void onTick(int remainingTime, int progress) {
        timerText.setText(getString(R.string.integer, remainingTime));
        timerBar.setProgress(progress);
    }

    @Override
    public void onTurnFinished() {
        timerBar.setProgress(0);
        Navigation.findNavController(this.root).navigate(R.id.action_playFragment_to_confirmFragment);
    }
}
