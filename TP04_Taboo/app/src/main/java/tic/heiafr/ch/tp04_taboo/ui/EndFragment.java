package tic.heiafr.ch.tp04_taboo.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import java.util.List;

import tic.heiafr.ch.tp04_taboo.R;
import tic.heiafr.ch.tp04_taboo.taboo.TabooManager;
import tic.heiafr.ch.tp04_taboo.taboo.Team;

public class EndFragment extends Fragment{
    private View root;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        this.root = inflater.inflate(R.layout.fragment_end, container, false);

        TabooManager tabooManager = TabooManager.getInstance(null);

        TextView goldTeamText = this.root.findViewById(R.id.team_gold);
        TextView goldScoreText = this.root.findViewById(R.id.score_gold);
        TextView silverTeamText = this.root.findViewById(R.id.team_silver);
        TextView silverScoreText = this.root.findViewById(R.id.score_silver);
        TextView bronzeTeamText = this.root.findViewById(R.id.team_bronze);
        TextView bronzeScoreText = this.root.findViewById(R.id.score_bronze);
        TextView failTeamText = this.root.findViewById(R.id.team_fail);
        TextView failScoreText = this.root.findViewById(R.id.score_fail);

        ImageView bronzeImage = this.root.findViewById(R.id.bronze_medal);
        ImageView failImage = this.root.findViewById(R.id.fail_medal);

        LinearLayout bronzeLayout = this.root.findViewById(R.id.layout_bronze);
        LinearLayout failLayout = this.root.findViewById(R.id.layout_fail);

        List<Team> teams = tabooManager.getTeams();

        goldTeamText.setText(getString(R.string.playing_team, teams.get(0).getName()));
        goldScoreText.setText(getString(R.string.score, teams.get(0).getScore()));

        silverTeamText.setText(getString(R.string.playing_team, teams.get(1).getName()));
        silverScoreText.setText(getString(R.string.score, teams.get(1).getScore()));

        if (teams.size() >= 3) {
            bronzeLayout.setVisibility(View.VISIBLE);
            bronzeImage.setVisibility(View.VISIBLE);
            bronzeTeamText.setText(getString(R.string.playing_team, teams.get(2).getName()));
            bronzeScoreText.setText(getString(R.string.score, teams.get(2).getScore()));
        }

        if (teams.size() >= 4) {
            failLayout.setVisibility(View.VISIBLE);
            failImage.setVisibility(View.VISIBLE);
            failTeamText.setText(getString(R.string.playing_team, teams.get(3).getName()));
            failScoreText.setText(getString(R.string.score, teams.get(3).getScore()));
        }
        return this.root;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
