package ch.heiafr.tic.mobapp.tp05_trivia.model.data;

/**
 * Data class to hold data about a category
 */
public class Category {

    //=== Attributes

    private String id;         // category id in the trivia api
    private String name;       // category name

    public Category(String id, String name) {
        this.id = id;
        this.name =name;
    }


    //=== Getters

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
