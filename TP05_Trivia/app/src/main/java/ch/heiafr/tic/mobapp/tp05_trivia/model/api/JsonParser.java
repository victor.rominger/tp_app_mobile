package ch.heiafr.tic.mobapp.tp05_trivia.model.api;

import android.text.Html;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Category;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;

public class JsonParser {
    private static final String FIELD_CATEGORIES = "trivia_categories";
    private static final String FIELD_CATEGORY = "category";
    private static final String FIELD_CORRECT_ANSWER = "correct_answer";
    private static final String FIELD_DIFFICULTY = "difficulty";
    private static final String FIELD_ID = "id";
    private static final String FIELD_INCORRECT_ANSWERS = "incorrect_answers";
    private static final String FIELD_NAME = "name";
    private static final String FIELD_QUESTION = "question";
    private static final String FIELD_RESULTS = "results";
    private static final String FIELD_TYPE = "type";

    public static List<Question> getQuestions(String content) {
        try {
            JSONArray jsonCategories = new JSONObject(content).getJSONArray(FIELD_RESULTS);
            List<Question> questions = new ArrayList<>();
            for (int i = 0; i < jsonCategories.length(); i++) {
                JSONObject c = jsonCategories.getJSONObject(i);
                String category = c.getString(FIELD_CATEGORY);
                String type = c.getString(FIELD_TYPE);
                String difficulty = c.getString(FIELD_DIFFICULTY);
                String question = Html.fromHtml(c.getString(FIELD_QUESTION)).toString();
                List<String> answers = new ArrayList<>();
                answers.add(Html.fromHtml(c.getString(FIELD_CORRECT_ANSWER)).toString());
                JSONArray jsonIncorrectAnswers = c.getJSONArray(FIELD_INCORRECT_ANSWERS);
                for (int j = 0; j < jsonIncorrectAnswers.length(); j++) {
                    answers.add(Html.fromHtml(jsonIncorrectAnswers.optString(j)).toString());
                }
                questions.add(new Question(category, type, difficulty, question, "0", answers));
            }
            return questions;
        } catch (JSONException e) {
            return new ArrayList<>();
        }
    }

    public static List<Category> getCategories(String content) {
        try {
            JSONArray jsonCategories = new JSONObject(content).getJSONArray(FIELD_CATEGORIES);
            List<Category> categories = new ArrayList<>();
            for (int i = 0; i < jsonCategories.length(); i++) {
                JSONObject c = jsonCategories.getJSONObject(i);
                categories.add(new Category(c.getString(FIELD_ID), c.getString(FIELD_NAME)));
            }
            return categories;
        } catch (JSONException e) {
            return new ArrayList<>();
        }
    }

}