package ch.heiafr.tic.mobapp.tp05_trivia.model.trivia;

import android.app.Application;
import android.os.CountDownTimer;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.List;
import java.util.Random;

import ch.heiafr.tic.mobapp.tp05_trivia.R;
import ch.heiafr.tic.mobapp.tp05_trivia.model.api.JsonParser;
import ch.heiafr.tic.mobapp.tp05_trivia.model.api.UrlGenerator;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Category;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;
import ch.heiafr.tic.mobapp.tp05_trivia.model.persistence.QuestionDao;
import ch.heiafr.tic.mobapp.tp05_trivia.model.persistence.QuestionDatabase;
import ch.heiafr.tic.mobapp.tp05_trivia.model.persistence.StatisticPreference;

/**
 * Repository handling the whole trivia logic and holding data
 */
public class TriviaRepository {
    public static final int ANSWER_NUMBER = 4;
    public static final int DEFAULT_CORRECT_ANSWER = 0;
    public static final String EASY = "easy";
    private static TriviaRepository INSTANCE = null;
    public static final int MEDAL_BRONZE = 4;
    public static final int MEDAL_GOLD = 9;
    public static final int MEDAL_SILVER = 6;
    public static final String MEDIUM = "medium";
    public static final int MODE_GAME = 1;
    public static final int MODE_HISTORY = 0;
    public static final int QUESTION_INTERVAL = 1500;
    public static final int QUESTION_NUMBER = 10;
    public static final String QUESTION_TYPE = "multiple";
    private static final String OPEN_TRIVIA_URL = "https://opentdb.com/api.php";
    private static final String OPEN_TRIVIA_CATEGORY_URL = "https://opentdb.com/api_category.php";
    private final Application application;
    private MutableLiveData<List<Category>> categories;
    private Category category;
    private MutableLiveData<Integer> corrects;
    private CountDownTimer countDownTimer;
    private int current;
    private MutableLiveData<Question> currentQuestion;
    private MutableLiveData<Boolean> gameEnded;
    private MutableLiveData<TriviaMedal> medal;
    private QuestionDao questionDao;
    private List<Question> questions;
    private final Random random = new Random();
    private MutableLiveData<String> textMedal;
    private StatisticPreference statisticPref;
    private final RequestQueue queue;

    private TriviaRepository(Application application) {
        this.application = application;
        this.queue = Volley.newRequestQueue(this.application);
        getDatabaseDaos();
        initLiveDatas();
    }

    public static TriviaRepository getInstance(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new TriviaRepository(application);
        }
        return INSTANCE;
    }

    public LiveData<List<Category>> getCategories() {
        return this.categories;
    }

    public LiveData<Question> getCurrentQuestion() {
        return this.currentQuestion;
    }

    public LiveData<Boolean> isGameEnded() {
        return this.gameEnded;
    }

    public int getCurrent() {
        return this.current;
    }

    public Category getCategory() {
        return this.category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public LiveData<Integer> getCorrects() {
        return this.corrects;
    }

    public LiveData<TriviaMedal> getMedal() {
        return this.medal;
    }

    public LiveData<String> getTextMedal() {
        return this.textMedal;
    }

    public void setCurrentQuestion(Question question) {
        this.currentQuestion.setValue(question);
    }

    public void cancelCountDownTimer() {
        CountDownTimer countDownTimer = this.countDownTimer;
        if (countDownTimer != null) {
            countDownTimer.cancel();
        }
    }

    public void fetchCategories() {
        StringRequest stringRequest =
                new StringRequest(Request.Method.GET, OPEN_TRIVIA_CATEGORY_URL,
                        response -> {
                            // Display the first 500 characters of the response string.
                            categories.setValue(JsonParser.getCategories(response));
                            for (Category category: categories.getValue()) {
                                Log.i("categories", category.getId());
                                Log.i("categories", category.getName());

                            }
                            Log.i("server reply", response);
                        }, error -> Log.e("error", error.toString()));
        queue.add(stringRequest);
    }

    public void initTriviaGame(int mode) {
        this.gameEnded.setValue(false);
        if (mode == MODE_HISTORY) {
            this.category = null;
            return;
        }
        this.current = -1;
        this.corrects.setValue(DEFAULT_CORRECT_ANSWER);

        fetchQuestions();
    }

    public boolean checkAnswer(String answer) {
        this.currentQuestion.getValue().setGivenAnswer(answer);
        saveAnswer();
        boolean result = answer.equals(this.currentQuestion.getValue().getCorrectAnswer());
        this.currentQuestion.getValue().setCorrect(result);
        if (result) {
            MutableLiveData<Integer> mutableLiveData = this.corrects;
            mutableLiveData.setValue(mutableLiveData.getValue() + 1);
        }
        this.statisticPref.updateQuestionAnswered(result);
        CountDownTimer timer = new CountDownTimer(QUESTION_INTERVAL, QUESTION_INTERVAL) {

            public void onTick(long l) {
            }

            public void onFinish() {
                nextQuestion();
            }
        };
        this.countDownTimer = timer;
        timer.start();
        return result;
    }

    public LiveData<List<Question>> getHistory() {
        return questionDao.getAll();
    }

    public void determineMedal() {
        if (((float) this.corrects.getValue()) >= MEDAL_GOLD) {
            this.medal.setValue(TriviaMedal.GOLD);
            this.textMedal.setValue(this.application.getString(R.string.gold_text));
            this.statisticPref.updateMedalGold();

        } else if (((float) this.corrects.getValue()) >= MEDAL_SILVER) {
            this.medal.setValue(TriviaMedal.SILVER);
            this.textMedal.setValue(this.application.getString(R.string.silver_text));
            this.statisticPref.updateMedalSilver();

        } else if (((float) this.corrects.getValue()) >= MEDAL_BRONZE) {
            this.medal.setValue(TriviaMedal.BRONZE);
            this.textMedal.setValue(this.application.getString(R.string.bronze_text));
            this.statisticPref.updateMedalBronze();

        } else {
            this.medal.setValue(TriviaMedal.NONE);
            this.textMedal.setValue(this.application.getString(R.string.nothing_text));
        }
        this.statisticPref.updateTriviaCompleted();
        this.statisticPref.updatePlayerRating();
    }

    public void resetCurrentQuestion() {
        this.currentQuestion = new MutableLiveData<>();
    }

    private void getDatabaseDaos() {
        this.questionDao = QuestionDatabase.getDatabase(this.application).questionDao();
        this.statisticPref = StatisticPreference.getInstance(this.application);
    }

    private void initLiveDatas() {
        this.categories = new MutableLiveData<>();
        this.currentQuestion = new MutableLiveData<>();
        this.gameEnded = new MutableLiveData<>();
        this.textMedal = new MutableLiveData<>();
        this.medal = new MutableLiveData<>();
        this.corrects = new MutableLiveData<>();
    }

    private void fetchQuestions() {
        UrlGenerator urlGenerator = new UrlGenerator(
                null, QUESTION_NUMBER, this.category.getId(), OPEN_TRIVIA_URL, null, QUESTION_TYPE);

        StringRequest stringRequest =
                new StringRequest(Request.Method.GET, urlGenerator.generateQuestionUrl(),
                        response -> {
                            // Display the first 500 characters of the response string.
                            questions = (JsonParser.getQuestions(response));
                            mixAnswers();
                            for (Question question : questions) {
                                Log.i("question", question.getQuestion());
                            }
                            nextQuestion();
                            Log.i("server reply", response);
                        }, error -> Log.e("error", error.toString()));
        queue.add(stringRequest);
    }

    private void saveAnswer() {
        QuestionDatabase.databaseWriteExecutor.execute(() -> {
            questionDao.insert(currentQuestion.getValue());
        });
    }

    private void mixAnswers() {
        for (Question q : this.questions) {
            int index = this.random.nextInt(ANSWER_NUMBER);
            q.getAnswers().add(index, q.getAnswers().remove(DEFAULT_CORRECT_ANSWER));
            q.setCorrectAnswer(String.valueOf(index));
        }
    }

    private void nextQuestion() {
        int i = this.current + 1;
        this.current = i;
        if (i < QUESTION_NUMBER) {
            setCurrentQuestion(this.questions.get(i));
        } else {
            this.gameEnded.setValue(true);
        }
    }
}
