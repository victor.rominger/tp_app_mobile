package ch.heiafr.tic.mobapp.tp05_trivia.ui.game;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Application;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

import ch.heiafr.tic.mobapp.tp05_trivia.R;
import ch.heiafr.tic.mobapp.tp05_trivia.databinding.FragmentGameBinding;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;
import ch.heiafr.tic.mobapp.tp05_trivia.model.trivia.TriviaRepository;
import ch.heiafr.tic.mobapp.tp05_trivia.ui.home.HomeFragmentDirections;

/**
 * Fragment handling the game view
 */
public class GameFragment extends Fragment {


    //=== Fragment's override

    FragmentGameBinding binding;
    GameViewModel gameViewModel;


    // Tell the system that this fragment has its own menu.
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    // Inflate the layout using DataBinding and keep a binding object reference which is used to directly access the views.
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_game, container, false);
        return binding.getRoot();
    }

    // Fragment's logic. Bind views to data and handle navigation.
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // creates a unique instance of the GameViewModel class
        gameViewModel = new ViewModelProvider(this, new GameViewModelFactory(getActivity().getApplication(), GameFragmentArgs.fromBundle(getArguments()).getMode())).get(GameViewModel.class);

        // sets data binding to automatically update the views when a LiveData variable of the ViewModel changes.
        binding.setViewModel(gameViewModel);
        binding.setLifecycleOwner(getViewLifecycleOwner());

        gameViewModel.getCurrentQuestion().observe(getViewLifecycleOwner(), question -> {
            updateUI(question);
            if (gameViewModel.getCategory() != null) {
                gameViewModel.setInterfaceLocked(false);
            }
        });
        gameViewModel.isGameEnded().observe(getViewLifecycleOwner(), (Observer) gameEnd -> {
            if ((Boolean) gameEnd) {
                NavDirections action = GameFragmentDirections.actionGameFragmentToResultFragment();
                Navigation.findNavController(view).navigate(action);
            }
        });

        // set the listener and handle click item callback
        AnswerListener listener = view1 -> {
            if (!gameViewModel.isInterfaceLocked()) {
                switch (view1.getId()) {
                    case R.id.card1:
                        showResult(binding.answer1, gameViewModel.checkAnswer("0"));
                        return;
                    case R.id.card2:
                        showResult(binding.answer2, gameViewModel.checkAnswer("1"));
                        return;
                    case R.id.card3:
                        showResult(binding.answer3, gameViewModel.checkAnswer("2"));
                        return;
                    case R.id.card4:
                        showResult(binding.answer4, gameViewModel.checkAnswer("3"));
                        return;
                    default:
                        Log.i("", "default");
                }
            }
        };
        binding.setListener(listener);
    }

    //=== Private methods

    private void setTitle(int current) {
        if (this.gameViewModel.getCategory() != null) {
            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.game_fragment_title, current, TriviaRepository.QUESTION_NUMBER));
            return;
        }
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.replay_fragment_title));
    }

    private void updateUI(Question question) {
        setTitle(this.gameViewModel.getReadableCurrent());
        this.binding.gameRoot.setVisibility(View.VISIBLE);
        this.binding.answer1.setBackgroundColor(Color.TRANSPARENT);
        this.binding.answer2.setBackgroundColor(Color.TRANSPARENT);
        this.binding.answer3.setBackgroundColor(Color.TRANSPARENT);
        this.binding.answer4.setBackgroundColor(Color.TRANSPARENT);
        int givenAnswer = Integer.parseInt(question.getGivenAnswer());
        if (givenAnswer == 0) {
            showResult(this.binding.answer1, question.isCorrect());
        } else if (givenAnswer == 1) {
            showResult(this.binding.answer2, question.isCorrect());
        } else if (givenAnswer == 2) {
            showResult(this.binding.answer3, question.isCorrect());
        } else if (givenAnswer == 3) {
            showResult(this.binding.answer4, question.isCorrect());
        }
        if (question.getDifficulty().equals(TriviaRepository.EASY)) {
            this.binding.difficulty.setBackgroundColor(getResources().getColor(R.color.colorEasy));
        } else if (question.getDifficulty().equals(TriviaRepository.MEDIUM)) {
            this.binding.difficulty.setBackgroundColor(getResources().getColor(R.color.colorMedium));
        } else {
            this.binding.difficulty.setBackgroundColor(getResources().getColor(R.color.colorHard));
        }
    }

    private void showResult(TextView view, boolean correct) {
        if (correct) {
            view.setBackgroundColor(getResources().getColor(R.color.colorRight));
            return;
        }
        view.setBackgroundColor(getResources().getColor(R.color.colorWrong));
        int colorRight = getResources().getColor(R.color.colorRight);
        int correctAnswer = Integer.parseInt(this.gameViewModel.getCurrentQuestion().getValue().getCorrectAnswer());
        if (correctAnswer == 0) {
            this.binding.answer1.setBackgroundColor(colorRight);
        } else if (correctAnswer == 1) {
            this.binding.answer2.setBackgroundColor(colorRight);
        } else if (correctAnswer == 2) {
            this.binding.answer3.setBackgroundColor(colorRight);
        } else if (correctAnswer == 3) {
            this.binding.answer4.setBackgroundColor(colorRight);
        }
    }

    //=== Public methods

    public static class GameViewModelFactory implements ViewModelProvider.Factory {
        private final Application application;
        private final int mode;

        public GameViewModelFactory(Application application, int mode) {
            this.application = application;
            this.mode = mode;
        }

        @Override
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            if (modelClass.isAssignableFrom(GameViewModel.class))
                return (T) new GameViewModel(this.application, this.mode);
            throw new IllegalArgumentException("Unknown ViewModel class");
        }
    }

    public interface AnswerListener {
        void onClick(View view);
    }
}
