package ch.heiafr.tic.mobapp.tp05_trivia.ui.profile;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import ch.heiafr.tic.mobapp.tp05_trivia.R;
import ch.heiafr.tic.mobapp.tp05_trivia.databinding.FragmentProfileBinding;

/**
 * Fragment handling the profile screen
 */
public class ProfileFragment extends Fragment {

    private FragmentProfileBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentProfileBinding fragmentProfileBinding = (FragmentProfileBinding) DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        this.binding = fragmentProfileBinding;
        return fragmentProfileBinding.getRoot();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.binding.setLifecycleOwner(getViewLifecycleOwner());
        this.binding.setViewModel((ProfileViewModel) new ViewModelProvider(this).get(ProfileViewModel.class));
    }

}
