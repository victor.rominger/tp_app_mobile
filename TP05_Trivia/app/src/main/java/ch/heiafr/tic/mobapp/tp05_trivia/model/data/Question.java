package ch.heiafr.tic.mobapp.tp05_trivia.model.data;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.util.Arrays;
import java.util.List;

/**
 * Data class to hold data about a question
 */
@Entity(tableName = "question_table")
public class Question {

    //=== Attributes

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @ColumnInfo(name = "category")
    private String category;          // category title
    @ColumnInfo(name = "type")
    private String type;              // multiple choice or true/false
    @ColumnInfo(name = "difficulty")
    private String difficulty;        // easy/medium/hard
    @ColumnInfo(name = "question")
    private String question;          // question title
    @ColumnInfo(name = "correctAnswer")
    private String correctAnswer;     // index of correct answer
    @ColumnInfo(name = "givenAnswer")
    private String givenAnswer = "-1";// index of given answer by the player
    @ColumnInfo(name = "correct")
    private boolean correct;          // player has correctly answered
    @ColumnInfo(name = "answers")
    private List<String> answers;     // list of possible answers

    public Question(String category, String type, String difficulty, String question, String correctAnswer, List<String> answers) {
        this.category = category;
        this.type = type;
        this.difficulty = difficulty;
        this.question = question;
        this.correctAnswer = correctAnswer;
        this.answers = answers;
    }

    //=== Getters and setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public String getType() {
        return type;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public String getQuestion() {
        return question;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }

    public void setCorrectAnswer(String index) {
        this.correctAnswer = index;
    }

    public String getGivenAnswer() {
        return givenAnswer;
    }

    public void setGivenAnswer(String givenAnswer) {
        this.givenAnswer = givenAnswer;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    public List<String> getAnswers() {
        return answers;
    }
}
