package ch.heiafr.tic.mobapp.tp05_trivia.ui.home;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.json.JSONException;

import java.util.List;
import java.util.concurrent.ExecutionException;

import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Category;
import ch.heiafr.tic.mobapp.tp05_trivia.model.trivia.TriviaRepository;

public class HomeViewModel extends AndroidViewModel {

    private final LiveData<List<Category>> categories;
    private final TriviaRepository triviaRepository;

    public HomeViewModel(Application application) {
        super(application);
        TriviaRepository instance = TriviaRepository.getInstance(application);
        this.triviaRepository = instance;
        instance.fetchCategories();
        this.categories = instance.getCategories();
    }

    public LiveData<List<Category>> getCategories() {
        return this.categories;
    }

    public void setCategory(Category category) {
        this.triviaRepository.setCategory(category);
    }
}
