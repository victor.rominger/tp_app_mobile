package ch.heiafr.tic.mobapp.tp05_trivia.model.api;

import android.util.Log;

public class UrlGenerator {

    String apiAddress;
    String token = "token=";
    String selected_category = "category=";
    String difficulty = "difficulty=";
    String type = "type=";
    String amount = "amount=";
    String separator = "&";
    String ask = "?";

    public UrlGenerator(String token, Integer amount, String categoryNumber, String apiAddress, String difficulty, String type) {
        this.apiAddress = apiAddress;
        this.amount += amount;
        this.token += token;
        this.selected_category += categoryNumber;
        this.difficulty += difficulty;
        this.type += type;
    }

    // Generate an url as follow using a token
    // https://opentdb.com/api.php?amount=10&category=9&difficulty=medium&type=multiple&token=5...
    public String generateQuestionUrlWithToken() {
        return (this.apiAddress
                + this.ask
                + this.amount
                + this.separator
                + this.selected_category
                + this.separator
                + this.difficulty
                + this.separator
                + this.type
                + this.separator
                + this.token);
    }

    public String generateQuestionUrl() {
        Log.i("test", this.apiAddress
                + this.ask
                + this.amount
                + this.separator
                + this.selected_category
                + this.separator
                + this.type);
        return (this.apiAddress
                + this.ask
                + this.amount
                + this.separator
                + this.selected_category
                + this.separator
                + this.type
        );

    }
}
