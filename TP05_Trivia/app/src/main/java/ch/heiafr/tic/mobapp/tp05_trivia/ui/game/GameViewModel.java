package ch.heiafr.tic.mobapp.tp05_trivia.ui.game;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import org.json.JSONException;

import java.util.concurrent.ExecutionException;

import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Category;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;
import ch.heiafr.tic.mobapp.tp05_trivia.model.trivia.TriviaRepository;

public class GameViewModel extends AndroidViewModel {

    private final LiveData<Question> currentQuestion;
    private final LiveData<Boolean> gameEnded;
    private boolean interfaceLocked = true;
    private final TriviaRepository triviaRepository;

    public GameViewModel(Application application, int mode) {
        super(application);
        TriviaRepository instance = TriviaRepository.getInstance(application);
        this.triviaRepository = instance;
        this.gameEnded = instance.isGameEnded();
        instance.cancelCountDownTimer();
        if (mode == TriviaRepository.MODE_GAME) {
            instance.resetCurrentQuestion();
        }
        this.currentQuestion = instance.getCurrentQuestion();
        instance.initTriviaGame(mode);
    }

    public LiveData<Question> getCurrentQuestion() {
        return this.currentQuestion;
    }

    public LiveData<Boolean> isGameEnded() {
        return this.gameEnded;
    }

    public int getReadableCurrent() {
        return this.triviaRepository.getCurrent() + 1;
    }

    public boolean isInterfaceLocked() {
        return this.interfaceLocked;
    }

    public void setInterfaceLocked(boolean interfaceLocked) {
        this.interfaceLocked = interfaceLocked;
    }

    public Category getCategory() {
        return this.triviaRepository.getCategory();
    }

    public boolean checkAnswer(String answer) {
        this.interfaceLocked = true;
        return this.triviaRepository.checkAnswer(answer);
    }
}
