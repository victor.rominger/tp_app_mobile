package ch.heiafr.tic.mobapp.tp05_trivia.model.persistence;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;
import java.util.List;

@Dao
public interface QuestionDao {

    @Query("SELECT * FROM question_table")
    LiveData<List<Question>> getAll();

    @Insert()
    void insert(Question question);
}
