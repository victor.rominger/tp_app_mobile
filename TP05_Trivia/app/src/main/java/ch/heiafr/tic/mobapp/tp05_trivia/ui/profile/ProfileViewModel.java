package ch.heiafr.tic.mobapp.tp05_trivia.ui.profile;

import android.app.Application;
import androidx.arch.core.util.Function;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import ch.heiafr.tic.mobapp.tp05_trivia.R;
import ch.heiafr.tic.mobapp.tp05_trivia.model.persistence.StatisticPreference;

public class ProfileViewModel extends AndroidViewModel {
    private final StatisticPreference statisticPref;

    public ProfileViewModel(Application application) {
        super(application);
        this.statisticPref = StatisticPreference.getInstance(application);
    }

    public LiveData<Integer> getCountGold() {
        return this.statisticPref.getTotalGold();
    }

    public LiveData<Integer> getCountSilver() {
        return this.statisticPref.getTotalSilver();
    }

    public LiveData<Integer> getCountBronze() {
        return this.statisticPref.getTotalBronze();
    }

    public LiveData<Integer> getCountTrivia() {
        return this.statisticPref.getTotalTrivia();
    }

    public LiveData<Integer> getCountAnswered() {
        return this.statisticPref.getTotalAnswered();
    }

    public LiveData<Integer> getCountCorrect() {
        return this.statisticPref.getTotalCorrect();
    }

    public LiveData<String> getPlayerRating() {
        return Transformations.map(this.statisticPref.getPlayerRating(), (Function) obj -> {
            if ((Float)obj != null) {
                return getApplication().getString(R.string.rating, new Object[]{Float.valueOf(((Float)obj).floatValue() * 100.0f)});
            }
            return getApplication().getString(R.string.rating, new Object[]{Float.valueOf(0.0f)});
        });
    }
}
