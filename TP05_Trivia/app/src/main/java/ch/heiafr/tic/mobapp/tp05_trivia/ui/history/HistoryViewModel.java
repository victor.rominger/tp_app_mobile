package ch.heiafr.tic.mobapp.tp05_trivia.ui.history;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;
import ch.heiafr.tic.mobapp.tp05_trivia.model.trivia.TriviaRepository;
import java.util.List;

public class HistoryViewModel extends AndroidViewModel {
    private final LiveData<List<Question>> history;
    private final TriviaRepository triviaRepository;

    public HistoryViewModel(Application application) {
        super(application);
        TriviaRepository instance = TriviaRepository.getInstance(application);
        this.triviaRepository = instance;
        this.history = instance.getHistory();
    }

    public LiveData<List<Question>> getHistory() {
        return this.history;
    }

    public void setQuestion(Question question) {
        this.triviaRepository.setCurrentQuestion(question);
    }
}