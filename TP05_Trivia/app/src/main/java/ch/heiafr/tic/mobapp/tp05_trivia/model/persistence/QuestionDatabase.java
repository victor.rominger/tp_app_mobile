package ch.heiafr.tic.mobapp.tp05_trivia.model.persistence;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import ch.heiafr.tic.mobapp.tp05_trivia.model.api.Converters;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;

@Database(entities = {Question.class}, version = 1, exportSchema = false)
@TypeConverters({Converters.class})
public abstract class QuestionDatabase extends RoomDatabase {
    private static final String DATABASE_NAME = "question_database";
    private static volatile QuestionDatabase INSTANCE = null;
    private static final int NUMBER_OF_THREADS = 4;
    public static final ExecutorService databaseWriteExecutor = Executors.newFixedThreadPool(NUMBER_OF_THREADS);

    public abstract QuestionDao questionDao();

    public static QuestionDatabase getDatabase(Context context) {
        if (INSTANCE == null) {
            synchronized (QuestionDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(), QuestionDatabase.class, DATABASE_NAME).build();
                }
            }
        }
        return INSTANCE;
    }
}
