package ch.heiafr.tic.mobapp.tp05_trivia.ui.history;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.Observer;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ch.heiafr.tic.mobapp.tp05_trivia.R;
import ch.heiafr.tic.mobapp.tp05_trivia.databinding.FragmentHistoryBinding;
import ch.heiafr.tic.mobapp.tp05_trivia.model.data.Question;
import ch.heiafr.tic.mobapp.tp05_trivia.model.trivia.TriviaRepository;

/**
 * Fragment handling the history screen
 */
public class HistoryFragment extends Fragment {

    private FragmentHistoryBinding binding;
    private HistoryViewModel viewModel;

    //=== Fragment's overrides

    // Inflate the layout using DataBinding and keep a binding object reference which is used to directly access the views.
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        FragmentHistoryBinding fragmentHistoryBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_history, container, false);
        this.binding = fragmentHistoryBinding;
        return fragmentHistoryBinding.getRoot();
    }

    // Fragment's logic. Bind views to data and handle navigation.
    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        this.viewModel = new ViewModelProvider(this).get(HistoryViewModel.class);
        RecyclerView recyclerView = this.binding.history;
        HistoryAdapter adapter = new HistoryAdapter(new ArrayList<>(), (view1, question) -> {
            viewModel.setQuestion(question);
            Navigation.findNavController(view1).navigate(HistoryFragmentDirections.actionHistoryFragmentToGameFragment(TriviaRepository.MODE_HISTORY));
        });
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        LiveData<List<Question>> history = this.viewModel.getHistory();
        LifecycleOwner viewLifecycleOwner = getViewLifecycleOwner();
        history.observe(viewLifecycleOwner, adapter::updateItems);
    }
}
