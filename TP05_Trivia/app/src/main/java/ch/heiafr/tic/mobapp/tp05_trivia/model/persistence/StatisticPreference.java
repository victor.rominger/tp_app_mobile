package ch.heiafr.tic.mobapp.tp05_trivia.model.persistence;

import android.app.Application;
import android.content.SharedPreferences;
import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import ch.heiafr.tic.mobapp.tp05_trivia.R;

public class StatisticPreference {
    private static StatisticPreference INSTANCE;
    private final Application application;
    private MutableLiveData<Float> playerRating;
    private final SharedPreferences sharedPref;
    private MutableLiveData<Integer> totalAnswered;
    private MutableLiveData<Integer> totalBronze;
    private MutableLiveData<Integer> totalCorrect;
    private MutableLiveData<Integer> totalGold;
    private MutableLiveData<Integer> totalSilver;
    private MutableLiveData<Integer> totalTrivia;

    private StatisticPreference(Application application) {
        this.application = application;
        this.sharedPref = application.getApplicationContext().getSharedPreferences(application.getString(R.string.pref_key_file), 0);
        initLiveDatas();
    }

    public static StatisticPreference getInstance(Application application) {
        if (INSTANCE == null) {
            INSTANCE = new StatisticPreference(application);
        }
        return INSTANCE;
    }

    private void initLiveDatas() {
        this.totalGold = new MutableLiveData<>(this.sharedPref.getInt(this.application.getString(R.string.pref_key_total_gold), 0));
        this.totalSilver = new MutableLiveData<>(this.sharedPref.getInt(this.application.getString(R.string.pref_key_total_silver), 0));
        this.totalBronze = new MutableLiveData<>(this.sharedPref.getInt(this.application.getString(R.string.pref_key_total_bronze), 0));
        this.totalTrivia = new MutableLiveData<>(this.sharedPref.getInt(this.application.getString(R.string.pref_key_total_trivia), 0));
        this.totalAnswered = new MutableLiveData<>(this.sharedPref.getInt(this.application.getString(R.string.pref_key_total_answered), 0));
        this.totalCorrect = new MutableLiveData<>(this.sharedPref.getInt(this.application.getString(R.string.pref_key_total_correct), 0));
        updatePlayerRating();
    }

    public LiveData<Integer> getTotalGold() {
        return this.totalGold;
    }

    public LiveData<Integer> getTotalSilver() {
        return this.totalSilver;
    }

    public LiveData<Integer> getTotalBronze() {
        return this.totalBronze;
    }

    public LiveData<Integer> getTotalTrivia() {
        return this.totalTrivia;
    }

    public LiveData<Integer> getTotalAnswered() {
        return this.totalAnswered;
    }

    public LiveData<Integer> getTotalCorrect() {
        return this.totalCorrect;
    }

    public LiveData<Float> getPlayerRating() {
        return this.playerRating;
    }

    public void updateQuestionAnswered(boolean correct) {
        MutableLiveData<Integer> mutableLiveData = this.totalAnswered;
        mutableLiveData.setValue(mutableLiveData.getValue() + 1);
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt(this.application.getString(R.string.pref_key_total_answered), this.totalAnswered.getValue());
        if (correct) {
            MutableLiveData<Integer> mutableLiveDataCorrect = this.totalCorrect;
            mutableLiveDataCorrect.setValue(mutableLiveDataCorrect.getValue() + 1);
            editor.putInt(this.application.getString(R.string.pref_key_total_correct), this.totalCorrect.getValue());
        }
        editor.apply();
    }

    public void updateMedalGold() {
        MutableLiveData<Integer> mutableLiveData = this.totalGold;
        mutableLiveData.setValue(mutableLiveData.getValue() + 1);
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt(this.application.getString(R.string.pref_key_total_gold), this.totalGold.getValue());
        editor.apply();
    }

    public void updateMedalSilver() {
        MutableLiveData<Integer> mutableLiveData = this.totalSilver;
        mutableLiveData.setValue(mutableLiveData.getValue() + 1);
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt(this.application.getString(R.string.pref_key_total_silver), this.totalSilver.getValue());
        editor.apply();
    }

    public void updateMedalBronze() {
        MutableLiveData<Integer> mutableLiveData = this.totalBronze;
        mutableLiveData.setValue(mutableLiveData.getValue() + 1);
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt(this.application.getString(R.string.pref_key_total_bronze), this.totalBronze.getValue());
        editor.apply();
    }

    public void updateTriviaCompleted() {
        MutableLiveData<Integer> mutableLiveData = this.totalTrivia;
        mutableLiveData.setValue(mutableLiveData.getValue() + 1);
        SharedPreferences.Editor editor = this.sharedPref.edit();
        editor.putInt(this.application.getString(R.string.pref_key_total_trivia), this.totalTrivia.getValue());
        editor.apply();
    }

    public void updatePlayerRating() {
        int answered = this.sharedPref.getInt(this.application.getString(R.string.pref_key_total_answered), 0);
        if (answered != 0) {
            this.playerRating = new MutableLiveData<>(((float) this.sharedPref.getInt(this.application.getString(R.string.pref_key_total_correct), 0)) / ((float) answered));
            Log.i("rating", Integer.toString(answered));
        } else {
            this.playerRating = new MutableLiveData<>(0.0f);
        }
    }
}
