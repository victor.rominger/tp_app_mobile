package tic.heiafr.ch.tp03_taboo.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import tic.heiafr.ch.tp03_taboo.R;
import tic.heiafr.ch.tp03_taboo.taboo.TabooManager;
import tic.heiafr.ch.tp03_taboo.taboo.Team;

/**
 * Activity charged to show the results of the finished taboo game
 *
 * @author Rafic Galli
 * @version 30.04.2020
 * @since 1.0
 */
public class EndActivity extends AppCompatActivity {

    /**
     *
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_end);

        TabooManager tabooManager = TabooManager.getInstance(null);

        TextView goldTeamText = findViewById(R.id.team_gold);
        TextView goldScoreText = findViewById(R.id.score_gold);
        TextView silverTeamText = findViewById(R.id.team_silver);
        TextView silverScoreText = findViewById(R.id.score_silver);
        TextView bronzeTeamText = findViewById(R.id.team_bronze);
        TextView bronzeScoreText = findViewById(R.id.score_bronze);
        TextView failTeamText = findViewById(R.id.team_fail);
        TextView failScoreText = findViewById(R.id.score_fail);

        ImageView bronzeImage = findViewById(R.id.bronze_medal);
        ImageView failImage = findViewById(R.id.fail_medal);

        LinearLayout bronzeLayout = findViewById(R.id.layout_bronze);
        LinearLayout failLayout = findViewById(R.id.layout_fail);

        List<Team> teams = tabooManager.getTeams();

        goldTeamText.setText(getString(R.string.playing_team, teams.get(0).getName()));
        goldScoreText.setText(getString(R.string.score, teams.get(0).getScore()));

        silverTeamText.setText(getString(R.string.playing_team, teams.get(1).getName()));
        silverScoreText.setText(getString(R.string.score, teams.get(1).getScore()));

        if (teams.size() >= 3) {
            bronzeLayout.setVisibility(View.VISIBLE);
            bronzeImage.setVisibility(View.VISIBLE);
            bronzeTeamText.setText(getString(R.string.playing_team, teams.get(2).getName()));
            bronzeScoreText.setText(getString(R.string.score, teams.get(2).getScore()));
        }

        if (teams.size() >= 4) {
            failLayout.setVisibility(View.VISIBLE);
            failImage.setVisibility(View.VISIBLE);
            failTeamText.setText(getString(R.string.playing_team, teams.get(3).getName()));
            failScoreText.setText(getString(R.string.score, teams.get(3).getScore()));
        }
    }

    /**
     *
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if(item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     *
     */
    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        finish();
    }

}
