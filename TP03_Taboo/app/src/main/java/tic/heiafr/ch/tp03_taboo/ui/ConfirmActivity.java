package tic.heiafr.ch.tp03_taboo.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import tic.heiafr.ch.tp03_taboo.R;
import tic.heiafr.ch.tp03_taboo.taboo.CardState;
import tic.heiafr.ch.tp03_taboo.taboo.Taboo;
import tic.heiafr.ch.tp03_taboo.taboo.TabooManager;

public class ConfirmActivity extends AppCompatActivity {
    TabooManager tabooManager;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm);

        tabooManager = TabooManager.getInstance(null);
        recyclerView = findViewById(R.id.confirm_recycler_view);
        List<Taboo> activatedCards = new ArrayList<>();
        tabooManager.getPlayedThisRound().forEach((card) -> {
            if ((card.getState() != null)) {
                activatedCards.add(card);
            }
        });
        if(activatedCards.size() != 0) {
            ConfirmAdapter confirmAdapter = new ConfirmAdapter(activatedCards,
                    position -> {
                        switch (tabooManager.getPlayedThisRound().get(position).getState()) {
                            case WON:
                                tabooManager.getPlayedThisRound().get(position)
                                        .setState(CardState.PASSED);
                                break;
                            case PASSED:
                                tabooManager.getPlayedThisRound().get(position)
                                        .setState(CardState.FAILED);
                                break;
                            case FAILED:
                                tabooManager.getPlayedThisRound().get(position)
                                        .setState(CardState.WON);
                                break;
                            default:
                                break;
                        }
                        recyclerView.getAdapter().notifyDataSetChanged();
                    }
            );
            recyclerView.setAdapter(confirmAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(ConfirmActivity.this));
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.confirm_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.validate_btn) {
            tabooManager.validateTurn();

            if (tabooManager.isGameOver()) {
                startActivity(new Intent(this, EndActivity.class));
            } else {
                Intent intent = new Intent(this, PlayActivity.class);
                intent.putExtra(HomeActivity.CODE_NEW_GAME, false);
                startActivity(intent);
            }
            return true;
        }
        return super.onContextItemSelected(item);
    }
}
